<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEditProfilesTable extends Migration
{

    public function up()
    {
        Schema::create('edit_profiles', function (Blueprint $table) {
            $table->id();
            $table->string('image')->nullable();
            $table->string('fullname')->nullable();
            $table->string('location')->nullable();
            $table->string('workname')->nullable();
            $table->string('slogan')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('edit_profiles');
    }
}
