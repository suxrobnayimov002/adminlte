<?php

namespace App\Repositories\admin;

use App\Models\Profile;

//use Your Model

/**
 * Class ProfileRepository.
 */
class ProfileRepository
{
    protected $entity;

    public function __construct(Profile $entity)
    {
        $this->entity = $entity;
    }

    public function all()
    {
       $messages = $this->entity->orderBy('id', 'desc')->paginate(5);
       return $messages;
    }

    public function findById($id)
    {
        $item = $this->entity->where('id', $id)->get();
        
        return $item;
    }

    public function store($request)
    {
        $item = $this->entity->create([
            'fullname' => $request['fullname'],
            'email' => $request['email'],
            'phone' => $request['phone'],
            'message' => $request['message'],
        ]);

        return $item;
    }
}
