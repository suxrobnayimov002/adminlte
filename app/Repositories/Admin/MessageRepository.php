<?php

namespace App\Repositories\admin;

use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use App\Models\Message;

//use Your Model

/**
 * Class MessageRepository.
 */
class MessageRepository
{
    protected $entity;

    public function __construct(Message $entity)
    {
        $this->entity = $entity;
    }

    public function all()
    {
       $messages = $this->entity->paginate(10);
       return $messages;
    }

    public function findById($id)
    {
        $item = $this->entity->where('id', $id)->get();
        return $item;
    }

    public function store($request)
    {
        $item = $this->entity->create([
            'fullname' => $request['full_name'],
            'email' => $request['email'],
            'phone' => $request['phone'],
            'message' => $request['message']
        ]);

        return $item;
    }
}
