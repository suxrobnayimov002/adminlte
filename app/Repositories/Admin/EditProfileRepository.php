<?php

namespace App\Repositories\admin;

use App\Models\EditProfile;
use MediaUploader;

//use Your Model

/**
 * Class EditProfileRepository.
 */
class EditProfileRepository
{
    protected $entity;

    public $media = null;

    public function __construct(EditProfile $entity)
    {
        $this->entity = $entity;
    }

    public function all()
    {
       $items = $this->entity->paginate(10);
       return $items;
    }

    public function findById($id)
    {
        $item = $this->entity->where('id', $id)->get();
        return $item;
    }

    public function store($request)
    {
        $item = $this->entity->create([
            'fullname' => $request['fullname'],
            'location' => $request['location'],
            'workname' => $request['workname'],
            'slogan'   => $request['slogan'],
        ]);

        if (isset($request['editprofile_logo'])) {
            $this->fileUpload($request['editprofile_logo'], $item);
        }

        return $item;
    }

    public function update($request, $id)
    {
        $item = $this->findById($id);

        if (isset($request['editprofile_logo'])) {
            $this->fileUpload($request['editprofile_logo'], $item[0]);
        }

        $item[0]->update([
            'fullname' => $request['fullname'],
            'location' => $request['location'],
            'workname' => $request['workname'],
            'slogan'   => $request['slogan'],
        ]);

        return $item;
    }

    public function fileUpload($image, $item)
    {
        $this->media = MediaUploader::fromSource($image)->useFilename(time())->toDirectory('editprofile')->upload();

        if (!is_null($this->media)) {
            $item->syncMedia($this->media->id, ['editprofile_logo']);
            return $this->entity->where('id', $item->id)->update(
                [
                    'editprofile_logo' => '/storage/editprofile/' . $item->getMedia('editprofile_logo')->first()->filename . '.' . $item->getMedia('editprofile_logo')->first()->extension
                ]
            );
        }
    }
}
