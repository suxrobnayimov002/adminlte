<?php

namespace App\Repositories\Admin;

use App\Models\Portfolio;
use MediaUploader;
// use Plank\Mediable\MediaUploader;

/**
 * Class PortfolioRepository.
 */
class PortfolioRepository
{
    protected $entity;

    public $media = null;

    public function __construct(Portfolio $entity)
    {
        $this->entity = $entity;
    }

    public function all()
    {
       $items = $this->entity->paginate(10);
       return $items;
    }

    public function findById($id)
    {
        $item = $this->entity->where('id', $id)->get();
        return $item;
    }

    public function store($request)
    {
        $item = $this->entity->create([
            'title' => $request['title'],
            'text' => $request['text'],
        ]);

        if (isset($request['image'])) {
            $this->fileUpload($request['image'], $item);
        }

        return $item;
    }

    public function update($request, $id)
    {
        $item = $this->findById($id);

        if (isset($request['image'])) {
            $this->fileUpload($request['image'], $item[0]);
        }
        // dd($item[0]);
        $item[0]->update([
            'title' => $request['title'],
            'text'  => $request['text']
        ]);

        return $item;
    }

    public function fileUpload($image, $item)
    {
        $this->media = MediaUploader::fromSource($image)->useFilename(time())->toDirectory('portfolio')->upload();

        if (!is_null($this->media)) {
            $item->syncMedia($this->media->id, ['portfolio_image']);
            return $this->entity->where('id', $item->id)->update(
                [
                    'image' => '/storage/portfolio/' . $item->getMedia('portfolio_image')->first()->filename . '.' . $item->getMedia('portfolio_image')->first()->extension
                ]
            );
        }
    }
}
