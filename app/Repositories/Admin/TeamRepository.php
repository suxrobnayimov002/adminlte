<?php

namespace App\Repositories\Admin;

use App\Models\Team;
use MediaUploader;
// use Plank\Mediable\MediaUploader;

/**
 * Class TeamRepository.
 */
class TeamRepository
{
    protected $entity;

    public $media = null;

    public function __construct(Team $entity)
    {
        $this->entity = $entity;
    }

    public function all()
    {
       $items = $this->entity->paginate(10);
       return $items;
    }

    public function findById($id)
    {
        $item = $this->entity->where('id', $id)->get();
        return $item;
    }

    public function store($request)
    {
        $item = $this->entity->create([
            'fullname' => $request['full_name'],
            'position' => $request['position'],
            'phone' => $request['phone']
        ]);

        if (isset($request['image'])) {
            $this->fileUpload($request['image'], $item);
        }

        return $item;
    }

    public function update($request, $id)
    {
        $item = $this->findById($id);

        if (isset($request['image'])) {
            $this->fileUpload($request['image'], $item[0]);
        }

        $item[0]->update([
            'fullname' => $request['full_name'],
            'position' => $request['position'],
            'phone' => $request['phone']
        ]);

        return $item;
    }

    public function fileUpload($image, $item)
    {
        $this->media = MediaUploader::fromSource($image)->useFilename(time())->toDirectory('teams')->upload();

        if (!is_null($this->media)) {
            $item->syncMedia($this->media->id, ['team_photos']);
            return $this->entity->where('id', $item->id)->update(
                [
                    'image' => '/storage/teams/' . $item->getMedia('team_photos')->first()->filename . '.' . $item->getMedia('team_photos')->first()->extension
                ]
            );
        }
    }

}
