<?php

namespace App\Repositories\Admin;

use App\Models\Service;
use MediaUploader;


//use Your Model

/**
 * Class ServiceRepository.
 */
class ServiceRepository
{
    protected $entity;

    public $media = null;

    public function __construct(Service $entity)
    {
        $this->entity = $entity;
    }

    public function all()
    {
       $items = $this->entity->paginate(10);
       return $items;
    }

    public function findById($id)
    {
        $item = $this->entity->where('id', $id)->get();
        return $item;
    }

    public function store($request)
    {
        $item = $this->entity->create([
            'service_title' => $request['service_title'],
            'service_text' => $request['service_text']
        ]);

        if (isset($request['service_logo'])) {
            $this->fileUpload($request['service_logo'], $item);
        }

        return $item;
    }

    public function update($request, $id)
    {
        $item = $this->findById($id);

        if (isset($request['service_logo'])) {
            $this->fileUpload($request['service_logo'], $item[0]);
        }
        // dd($item[0]);
        $item[0]->update([
            'service_title' => $request['service_title'],
            'service_text' => $request['service_text']
        ]);

        return $item;
    }

    public function fileUpload($image, $item)
    {
        $this->media = MediaUploader::fromSource($image)->useFilename(time())->toDirectory('service')->upload();

        if (!is_null($this->media)) {
            $item->syncMedia($this->media->id, ['service_logo']);
            return $this->entity->where('id', $item->id)->update(
                [
                    'service_logo' => '/storage/service/' . $item->getMedia('service_logo')->first()->filename . '.' . $item->getMedia('service_logo')->first()->extension
                ]
            );
        }
    }
}
