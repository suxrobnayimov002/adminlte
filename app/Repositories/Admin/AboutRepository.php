<?php

namespace App\Repositories\Admin;

use App\Models\About;
use MediaUploader;
// use Plank\Mediable\MediaUploader;

/**
 * Class AboutRepository.
 */
class AboutRepository
{
    protected $entity;

    public $media = null;

    public function __construct(About $entity)
    {
        $this->entity = $entity;
    }

    public function all()
    {
       $abouts = $this->entity->paginate(10);
       return $abouts;
    }

    public function findById($id)
    {
        $item = $this->entity->where('id', $id)->get();
        return $item;
    }

    public function store($request)
    {
        $item = $this->entity->create([
            'textshort' => $request['text_short'],
            'image' => $request['image'],
            'textall' => $request['text_all']
        ]);

        if (isset($request['image'])) {
            $this->logoUpload($request['image'], $item);
        }

        return $item;
    }

    public function update($request, $id)
    {
        $item = $this->findById($id);

        if (isset($request['image'])) {
            $this->logoUpload($request['image'], $item[0]);
        }

        $item[0]->update([
            'textshort' => $request['text_short'],
            'textall' => $request['text_all']
        ]);


        return $item;
    }

    public function logoUpload($image, $item)
    {
        $this->media = MediaUploader::fromSource($image)->useFilename(time())->toDirectory('abouts')->upload();

        if (!is_null($this->media)) {
            $item->syncMedia($this->media->id, ['about_image']);
            return $this->entity->where('id', $item->id)->update(
                [
                    'image' => '/storage/abouts/' . $item->getMedia('about_image')->first()->filename . '.' . $item->getMedia('about_image')->first()->extension
                ]
            );
        }
    }

}
