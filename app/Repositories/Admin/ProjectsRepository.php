<?php

namespace App\Repositories\Admin;

use App\Models\Project;
use MediaUploader;

//use Your Model

/**
 * Class ProjectsRepository.
 */
class ProjectsRepository
{
    protected $entity;

    public $media = null;

    public function __construct(Project $entity)
    {
        $this->entity = $entity;
    }

    public function all()
    {
       $items = $this->entity->paginate(10);
       return $items;
    }

    public function findById($id)
    {
        $item = $this->entity->where('id', $id)->get();
        return $item;
    }

    public function store($request)
    {
        $item = $this->entity->create([
            'pr_title' => $request['pr_title'],
            'pr_text' => $request['pr_text'],
        ]);

        if (isset($request['pr_image'])) {
            $this->fileUpload($request['pr_image'], $item);
        }

        return $item;
    }

    public function update($request, $id)
    {
        $item = $this->findById($id);

        if (isset($request['pr_image'])) {
            $this->fileUpload($request['pr_image'], $item[0]);
        }
        // dd($item[0]);
        $item[0]->update([
            'pr_title' => $request['pr_title'],
            'pr_text'  => $request['pr_text']
        ]);

        return $item;
    }

    public function fileUpload($image, $item)
    {
        $this->media = MediaUploader::fromSource($image)->useFilename(time())->toDirectory('project')->upload();

        if (!is_null($this->media)) {
            $item->syncMedia($this->media->id, ['project_image']);
            return $this->entity->where('id', $item->id)->update(
                [
                    'pr_image' => '/storage/project/' . $item->getMedia('project_image')->first()->filename . '.' . $item->getMedia('project_image')->first()->extension
                ]
            );
        }
    }
}
