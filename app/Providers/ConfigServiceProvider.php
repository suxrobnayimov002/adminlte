<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ConfigServiceProvider extends ServiceProvider
{
    public function register()
    {
        config([
			'laravellocalization.supportedLocales' => [
				'ru'  => array( 'name' => 'Russian', 'script' => 'Cyrl', 'native' => 'Рус' ),
				'en'  => array( 'name' => 'English', 'script' => 'Latn', 'native' => 'Eng' ),
			],

			'laravellocalization.useAcceptLanguageHeader' => true,

			'laravellocalization.hideDefaultLocaleInURL' => true
		]);
    }

    public function boot()
    {
        //
    }
}
