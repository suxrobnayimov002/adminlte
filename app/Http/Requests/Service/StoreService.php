<?php

namespace App\Http\Requests\Service;

use Illuminate\Foundation\Http\FormRequest;

class StoreService extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'service_title' => 'nullable|min:10|cyrillic',
            'service_text'  => 'nullable|min:20|cyrillic',
            'service_logo'  => 'nullable'
        ];
    }
}
