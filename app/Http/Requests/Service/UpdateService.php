<?php

namespace App\Http\Requests\Service;

use Illuminate\Foundation\Http\FormRequest;

class UpdateService extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'service_title' => 'nullable|min:10|regex:/^[a-zA-ZÑñ\s]+$/',
            'service_text'  => 'nullable|min:20|regex:/^[a-zA-ZÑñ\s]+$/',
            'service_logo'  => 'nullable'
        ];
    }
}
