<?php

namespace App\Http\Requests\TeamWork;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTeamWork extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'position'  => 'nullable',
            'image'     => 'nullable',
            'phone'     => 'numeric|regex:/^[-0-9\+]+$/',
            'full_name' => 'nullable',
            'ordered'   => 'nullable',
        ];
    }
}
