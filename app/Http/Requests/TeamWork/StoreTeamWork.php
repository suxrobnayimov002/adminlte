<?php

namespace App\Http\Requests\TeamWork;

use Illuminate\Foundation\Http\FormRequest;

class StoreTeamWork extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'position'  => 'nullable|regex:/^[А-Яа-яЁё]$/u',
            'image'     => 'nullable',
            'phone'     => 'numeric|regex:/^[-0-9\+]+$/',
            'full_name' => 'nullable|regex:/^[А-Яа-яЁё]$/u',
            'ordered'   => 'nullable|regex:/^[А-Яа-яЁё]$/u',
        ];
    }
}
