<?php

namespace App\Http\Requests\Projects;

use Illuminate\Foundation\Http\FormRequest;

class StoreProject extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'pr_title' => 'nullable',
            'pr_text'  => 'nullable',
            'pr_image' => 'nullable'
        ];
    }
}
