<?php

namespace App\Http\Requests\Message;

use Illuminate\Foundation\Http\FormRequest;

class StoreMessage extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'fullname'  => 'nullable|regex:/^[А-Яа-яЁё]$/u',
            'email'     => 'nullable',
            'phone'     => 'numeric|regex:/^[-0-9\+]+$/',
            'message'   => 'nullable'
        ];
    }
}
