<?php

namespace App\Http\Requests\EditProfile;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProfile extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
           'image'    => 'nullable',
           'fullname' => 'nullable',
           'location' => 'nullable',
           'workname' => 'nullable',
           'slogan'   => 'nullable'
        ];
    }
}
