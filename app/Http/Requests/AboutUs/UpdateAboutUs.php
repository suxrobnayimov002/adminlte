<?php

namespace App\Http\Requests\AboutUs;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAboutUs extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'textshort' => 'nullable',
            'image'     => 'nullable',
            'textall'   => 'nullable'
        ];
    }
}
