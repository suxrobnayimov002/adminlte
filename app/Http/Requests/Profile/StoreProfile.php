<?php

namespace App\Http\Requests\Profile;

use Illuminate\Foundation\Http\FormRequest;

class StoreProfile extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'fullname'  => 'nullable',
            'email'     => 'nullable',
            'phone'     => 'numeric|regex:/^[-0-9\+]+$/|min:10|max:14',
            'message'   => 'nullable'
        ];
    }
}
