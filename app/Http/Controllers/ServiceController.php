<?php

namespace App\Http\Controllers;

use App\Models\Service;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    public function service()
    {
        $services = Service::orderByDesc('created_at')->paginate(20);
        return view('client.service.index', compact('services'));
    }
}
