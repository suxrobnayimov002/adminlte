<?php

namespace App\Http\Controllers;

use App\Models\About;
use Illuminate\Http\Request;

class AboutUsController extends Controller
{
    public function about(){

        $abouts = About::orderByDesc('created_at')->paginate(20);
        return view('client.about-us.index', compact('abouts'));

    }
}
