<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Team;

class TeamController extends Controller
{
    public function team()
    {
        $teams = Team::orderByDesc('created_at')->paginate(20);
        return view('client.team.index', compact('teams'));
    }
}
