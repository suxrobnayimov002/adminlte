<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Profile\StoreProfile;
use App\Models\Profile;
use App\Repositories\Admin\ProfileRepository;

class ProfileController extends Controller
{   

    protected $repo;

    public function __construct(ProfileRepository $repo)
    {
        $this->repo = $repo;
    }

    public function index()
    {
        $messages = $this->repo->all();
        return view('admin.profile.index', compact('messages'));
    }

    public function store(StoreProfile $request)
    {
        $this->repo->store($request->all());
        return redirect()->route('client.contact.index');
    }

    public function destroy($id)
    {
        Profile::where('id', $id)->delete();
        return redirect()->route('profile.index')
                        ->with ('success','Lesson deleted successfully');
    }

}
