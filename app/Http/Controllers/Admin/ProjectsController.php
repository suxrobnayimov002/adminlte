<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Projects\StoreProject;
use App\Http\Requests\Projects\UpdateProject;
use App\Repositories\Admin\ProjectsRepository;
use App\Models\Project;
use Illuminate\Http\Request;

class ProjectsController extends Controller
{
    protected $repo;

    public function __construct(ProjectsRepository $repo)
    {
        $this->repo = $repo;
    }

    public function index()
    {
        $projects = $this->repo->all();
        return view('admin.project.index', compact('projects'));
    }

    public function create()
    {
        return view('admin.project.create');
    }

    public function store(StoreProject $request)
    {
        $this->repo->store($request->all());
        return redirect()->route('projects.index');
    }

    public function show($id)
    {
        $item = $this->repo->findById($id);
        return view('admin.project.show', compact('item'));
    }

    public function edit($id)
    {
        $item = $this->repo->findById($id);
        return view('admin.project.edit', compact('item'));
    }

    public function update(UpdateProject $request, $id)
    {
        $this->repo->update($request->all(), $id);
        return redirect()->route('projects.index');
    }

    public function destroy($id)
    {
        Project::where('id', $id)->delete();
        return redirect()->route('projects.index')
                        ->with('success','Lesson deleted successfully');
    }
}
