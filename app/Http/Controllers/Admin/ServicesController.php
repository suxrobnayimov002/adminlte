<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Service\StoreService;
use App\Http\Requests\Service\UpdateService;
use App\Repositories\Admin\ServiceRepository;
use App\Models\Service;

use Illuminate\Http\Request;

class ServicesController extends Controller
{
    protected $repo;

    public function __construct(ServiceRepository $repo)
    {
        $this->repo = $repo;
    }

    public function index()
    {
        $services = $this->repo->all();
        return view('admin.service.index', compact('services'));
    }

    public function create()
    {
        return view('admin.service.create');
    }

    public function store(StoreService $request)
    {
        $this->repo->store($request->all());
        return redirect()->route('service.index');
    }

    public function show($id)
    {
        $item = $this->repo->findById($id);
        return view('admin.service.show', compact('item'));
    }

    public function edit($id)
    {
        $item = $this->repo->findById($id);
        return view('admin.service.edit', compact('item'));
    }

    public function update(UpdateService $request, $id)
    {
        $this->repo->update($request->all(), $id);
        return redirect()->route('service.index');
    }

    public function destroy($id)
    {
        Service::where('id', $id)->delete();
        return redirect()->route('service.index')
                        ->with('success','Lesson deleted successfully');
    }
}
