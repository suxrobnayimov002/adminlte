<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\TeamWork\StoreTeamWork;
use App\Http\Requests\TeamWork\UpdateTeamWork;
use App\Models\Team;
use App\Repositories\Admin\TeamRepository;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    protected $repo;

    public function __construct(TeamRepository $repo)
    {
        $this->repo = $repo;
    }

    public function index()
    {
        $teams = $this->repo->all();
        return view('admin.teams.index', compact('teams'));
    }

    public function create()
    {
        return view('admin.teams.create');
    }

    public function store(StoreTeamWork $request)
    {
        $this->repo->store($request->all());
        return redirect()->route('teams.index');
    }

    public function show($id)
    {
        $team = $this->repo->findById($id);
        return view('admin.teams.show', compact('team'));
    }

    public function edit($id)
    {
        $team = $this->repo->findById($id);
        return view('admin.teams.edit', compact('team'));
    }

    public function update(UpdateTeamWork $request, $id)
    {
        $this->repo->update($request->all(), $id);
        return redirect()->route('teams.index');
    }

    public function destroy($id)
    {
        Team::where('id', $id)->delete();
        return redirect()->route('teams.index')
                        ->with('success','Lesson deleted successfully');
    }
}
