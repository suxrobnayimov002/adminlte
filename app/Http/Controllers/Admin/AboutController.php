<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\AboutUs\StoreAboutUs;
use App\Http\Requests\AboutUs\UpdateAboutUs;
use App\Models\About;
use App\Repositories\Admin\AboutRepository;

class AboutController extends Controller
{
    protected $repo;

    public function __construct(AboutRepository $repo)
    {
        $this->repo = $repo;
    }

    public function index()
    {
        $abouts = $this->repo->all();
        return view('admin.abouts.index', compact('abouts'));
    }

    public function create()
    {
        return view('admin.abouts.create');
    }

    public function store(StoreAboutUs $request)
    {
        $this->repo->store($request->all());
        return redirect()->route('abouts.index');
    }

    public function show($id)
    {
        $item = $this->repo->findById($id);
        return view('admin.abouts.show', compact('item'));
    }

    public function edit($id)
    {
        $item = $this->repo->findById($id);
        return view('admin.abouts.edit', compact('item'));
    }

    public function update(UpdateAboutUs $request, $id)
    {
        $this->repo->update($request->all(), $id);
        return redirect()->route('abouts.index');
    }

    public function destroy($id)
    {
        About::where('id', $id)->delete();
        return redirect()->route('abouts.index')
                        ->with('success','Lesson deleted successfully');
    }
}
