<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Portfolio\StorePortfolio;
use App\Http\Requests\Portfolio\UpdatePortfolio;
use App\Models\Portfolio;
use App\Repositories\Admin\PortfolioRepository;
use Illuminate\Http\Request;

class PortfolioController extends Controller
{
    protected $repo;

    public function __construct(PortfolioRepository $repo)
    {
        $this->repo = $repo;
    }

    public function index()
    {
        $portfolios = $this->repo->all();
        return view('admin.portfolio.index', compact('portfolios'));
    }

    public function create()
    {
        return view('admin.portfolio.create');
    }

    public function store(StorePortfolio $request)
    {
        $this->repo->store($request->all());
        return redirect()->route('portfolio.index');
    }

    public function show($id)
    {
        $item = $this->repo->findById($id);
        return view('admin.portfolio.show', compact('item'));
    }

    public function edit($id)
    {
        $item = $this->repo->findById($id);
        return view('admin.portfolio.edit', compact('item'));
    }

    public function update(UpdatePortfolio $request, $id)
    {
        $this->repo->update($request->all(), $id);
        return redirect()->route('portfolio.index');
    }

    public function destroy($id)
    {
        Portfolio::where('id', $id)->delete();
        return redirect()->route('portfolio.index')
                        ->with('success','Lesson deleted successfully');
    }
}
