<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\EditProfile\UpdateProfile;
use App\Models\EditProfile;
use App\Repositories\admin\EditProfileRepository;

class EditProfileController extends Controller
{
    protected $repo;

    public function __construct(EditProfileRepository $repo)
    {
        $this->repo = $repo;
    }

    public function index()
    {
        $services = $this->repo->all();
        return view('admin.edit-profile.index', compact('services'));
    }

    public function edit($id)
    {
        $item = $this->repo->findById($id);
        return view('admin.edit-profile.edit', compact('item'));
    }

    public function update(UpdateProfile $request, $id)
    {
        $this->repo->update($request->all(), $id);
        return redirect()->route('edit-profile.index');
    }

    public function destroy($id)
    {
        EditProfile::where('id', $id)->delete();
        return redirect()->route('edit-profile.index')
                        ->with('success','Lesson deleted successfully');
    }
}
