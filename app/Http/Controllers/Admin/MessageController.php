<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Message\StoreMessage;
use App\Repositories\Admin\PortfolioRepository;

class MessageController extends Controller
{

    protected $repo;

    public function __construct(PortfolioRepository $repo)
    {
        $this->repo = $repo;
    }

    public function index()
    {
        $messages = $this->repo->all();
        return view('admin.profile.index', compact('messages'));
    }

    public function store(StoreMessage $request)
    {
        $this->repo->store($request->all());
        return redirect()->route('message.index');
    }

}
