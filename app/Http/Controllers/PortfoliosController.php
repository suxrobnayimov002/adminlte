<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Portfolio;

class PortfoliosController extends Controller
{
    public function portfolio()
    {

        $portfolios = Portfolio::orderByDesc('created_at')->paginate(20);
        return view('client.portfolio.index', compact('portfolios'));

    }
}
