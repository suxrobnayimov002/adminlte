<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    public function project()
    {
        $projects = Project::orderByDesc('created_at')->paginate(20);
        return view('client.projects.index', compact('projects'));

    }
}
