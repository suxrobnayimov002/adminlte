<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    protected $redirectTo = RouteServiceProvider::HOME;
}
