<?php

namespace App\Http\Controllers;

use App\Mail\TestMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public function sendEmail()
    {
        $details = [
            'title' => 'Hurmatli',
            'body' => 'Cтроител proyektidan sizga email xabar keldi'
        ];

        $senders = [
            "suxrobnayimov002@gmail.com",
            "phpartisan.aaaa@gmail.com",
            "mahmanov@gmail.com"
        ];

        foreach($senders as $sender){
            Mail::to($sender)->send(new TestMail($details));
        }

        return "Email success send";

    }
}
