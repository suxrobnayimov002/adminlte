<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Plank\Mediable\Mediable;

class Project extends Model
{
    use HasFactory;
    use Mediable;

    protected $table = 'projects';

    protected $fillable = [
        'pr_title',
        'pr_text',
        'pr_image'
    ];

}
