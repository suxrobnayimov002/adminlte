<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Plank\Mediable\Mediable;


class Team extends Model
{
    use HasFactory;
    use Mediable;

    protected $table = 'teams';

    protected $fillable = [
        'fullname',
        'position',
        'image',
        'phone'
    ];

}
