<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EditProfile extends Model
{
    use HasFactory;

    protected $table = 'edit_profiles';

    protected $fillable = [
        'image',
        'fullname',
        'location',
        'workname',
        'slogan',
    ];
}
