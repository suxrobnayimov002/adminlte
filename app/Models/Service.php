<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Plank\Mediable\Mediable;

class Service extends Model
{
    use HasFactory;
    use Mediable;

    protected $table = 'services';
    
    protected $fillable = [
        'service_title',
        'service_text',
        'service_logo'
    ];

}
