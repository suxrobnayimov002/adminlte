<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Plank\Mediable\Mediable;

class Index extends Model
{
    use HasFactory;
    use Mediable;

    protected $table = 'index';

    protected $fillable = [
        'title',
        'text',
        'image'
    ];
}
