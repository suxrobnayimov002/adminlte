<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Plank\Mediable\Mediable;

class Portfolio extends Model
{
    use HasFactory;
    use Mediable;

    protected $table = 'portfolio';

    protected $fillable = [
        'title',
        'text',
        'image'
    ];

}
