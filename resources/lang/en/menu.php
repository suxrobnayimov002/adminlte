<?php
    return[
        'home'							=> 'Home',
        'about_us'                      => 'About us',
        'my-service'                    => 'My service',
        'portfolio'                     => 'Portfolio',
        'portfolio_all'                 => 'More Portfolio',
        'teams'                         => 'Teams',
        'contact'                       => 'Contact',
        'builder'                       => 'Builder',
        'projects'                      => 'Projects',
        'our-team'                      => 'Our team',
        'newslatter'                    => 'NEWSLATTER',
        'newslatter_text'               => 'SIGNUP NEWSLATTER',
        'useful_links'                  => 'Useful links',
        'send_message'                  => 'Send a message',
        'finished_project'			    => 'Projects Finished',
        'happy_clients'				    => 'Happy clients',
        'hours_worked'					=> 'Hours Worked',
        'process' 						=> 'Project process',


        'profile'                       => 'Профиль',
        'change_password'               => 'Изменить пароль',
        'multilevel'                    => 'Многоуровневое меню',
        'level_one'                     => 'Уровень 1',
        'level_two'                     => 'Уровень 2',
        'level_three'                   => 'Уровень 3',
        'labels'                        => 'Метки',
        'important'                     => 'Важно',
        'warning'                       => 'Внимание',
        'information'                   => 'Информация',
    ]
?>
