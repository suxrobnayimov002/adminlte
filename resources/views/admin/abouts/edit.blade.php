@extends('admin.layouts.main')

@section('title', __('Янги қўшилган маълумотни таҳрирлаш'))
<link rel="stylesheet" href="{{ asset('/vendor/summernote/summernote-bs4.css') }}">

@section('content_header')
    <h4>{{ __('Янги қўшилган маълумотни таҳрирлаш') }}</h4>
@stop

@section('content')
    <form action="{{ route('abouts.update', $item[0]->id) }}" method="POST" enctype="multipart/form-data">
        {{-- <?php dd($item[0]->id); ?> --}}
        @csrf()
        @method('PUT')
        <div class="form-group">
            <label>Қисқача маълумот киритинг</label>
            <textarea class="textarea form-control summernote" placeholder="Place some text here"
                style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                name="text_short">{{ $item[0]->textshort }}</textarea>
        </div>
        <div class="form-group">
            <label>Барча маълумот</label>
            <textarea class="textarea form-control summernote" placeholder="Place some text here"
                style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                name="text_all">{{ $item[0]->textall }}</textarea>
            <span class="text-danger">{{ $errors->first('text_all') }}</span>
        </div>
        <div class="form-group">
            <label>Расм</label>
            <div class="custom-file">
                <input type="file" name="image" class="custom-file-input" value="{{ $item[0]->image }}" id="customFile">
                <label class="custom-file-label" for="customFile">Расмни юклаш</label>
            </div>
        </div>

        <button type="submit" class="btn btn-md btn-primary float-right"><i
                class="fa fa-check"></i>&nbsp;&nbsp;{{ __('Маълумотни сақлаш') }}</button>
    </form>
@stop
@section('js')
    <script src="{{ asset('/vendor/summernote/summernote-bs4.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.summernote').summernote({
                height: 300
            });
        });
    </script>
@stop
