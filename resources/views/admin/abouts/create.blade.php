@extends('admin.layouts.main')

@section('title', 'Аъзо қўшиш')
<link rel="stylesheet" href="{{ asset('/vendor/summernote/summernote-bs4.css') }}">

@section('content_header')
    <h4>Аъзо қўшиш</h4>
@stop

@section('content')

    <form action="{{ route('abouts.store') }}" method="post" enctype="multipart/form-data">
        @csrf()
        <div class="form-group">
            <label>Қисқача маълумот киритинг</label>
            <textarea id="editor1" class="textarea form-control summernote" placeholder="Place some text here"
                style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                name="text_short">
                </textarea>
            {{-- <input class="form-control" name="text_short"> --}}
            <span class="text-danger">{{ $errors->first('text_short') }}</span>
        </div>
        <div class="form-group">
            <label>Барча маълумот</label>
            {{-- <input class="form-control" name="text_all"> --}}
            <textarea id="editor1" class="textarea form-control summernote" placeholder="Place some text here"
                style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                name="text_all">
            </textarea>
            <span class="text-danger">{{ $errors->first('text_all') }}</span>
        </div>
        <div class="form-group">
            <label>Расм</label>
            <div class="custom-file">
                <input type="file" name="image" class="custom-file-input" id="customFile">
                <label class="custom-file-label" for="customFile">Расмни юклаш</label>
              </div>
        </div>
        <button type="submit" class="btn btn-primary btn-md float-right"><i class="fa fa-check"></i>
            {{ __('Маълумотни сақлаш') }}</button>
    </form>
@stop
@section('js')
    <script src="{{ asset('/vendor/summernote/summernote-bs4.min.js') }}"></script>
    <script src="{{ asset('js/bs-custom-file-input.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.summernote').summernote({
                height: 300
            });
        });
    </script>
@stop
