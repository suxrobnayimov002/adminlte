@extends('admin.layouts.main')

@section('title', 'Биз ҳақимизда')

@section('content_header')
<h4>Биз ҳақимизда</h4>
@stop

@section('content')
<div class="card card-outline card-info">
    <div class="card-header">
        <h3 class="card-title">
            <a href="{{ route('abouts.create') }}" class="btn btn-primary btn-md float-right"><span class="fas fa-fw fa-plus"></span>Маълумот қўшиш
            </a>
        </h3>
    </div>
    <div class="card-body pad">
        <div class="mb-3">
            <table cellpadding="0" cellspacing="0" id="example" class="table table-bordered table-striped table-condensed">
                <thead>
                    <tr class="text-info">
                        <th>№</th>
                        <th>Расм</th>
                        <th>Қисқача маълумот</th>
                        <th>Барча маълумот</th>
                        <th class="actions">Амаллар</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1;?>
                    @foreach($abouts as $item)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td style="width: 140px;"><img src="{{ $item->image }}" alt="" width="100%" height="100"></td>
                        <td>{{ $item->textshort }} </td>
                        <td>{{ $item->textall }} </td>
                        <td>
                            <a href="{{ route('abouts.edit', $item->id) }}" class="btn btn-info btn-sm">
                                <span class="fas fa-fw fa-pencil-alt"></span>
                            </a>
                            <form action="{{ route('abouts.destroy',  $item->id) }}" method="post" onsubmit="return confirm('Сиз ростдан ҳам ушбу маълумотни ўчиришни хохлайсизми ?')">
                                @csrf
                                @method('delete')
                                <button type="submit" class="btn btn-sm btn-danger">
                                    <span class="fas fa-fw fa-trash-alt"></span>
                                </button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    {{ $abouts->links() }}
</div>
@stop
@section('js')
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });

</script>
@stop
