@extends('admin.layouts.main')

@section('title', __('Янги қўшилган маълумотни таҳрирлаш'))
<link rel="stylesheet" href="{{ asset('/vendor/summernote/summernote-bs4.css') }}">

@section('content_header')
    <h4>{{ __('Янги қўшилган маълумотни таҳрирлаш') }}</h4>
@stop

@section('content')
    <form action="{{ route('portfolio.update', $item[0]->id) }}" method="POST" enctype="multipart/form-data">
        {{-- <?php dd($item[0]->text); ?> --}}
        @csrf()
        @method('PUT')
        <div class="form-group">
            <label>Сарлавҳа номи</label>
            <input class="form-control" name="title" value="{{ $item[0]->title }}">
        </div>
        <div class="form-group">
            <label>Матн киритинг</label>
            <textarea class="textarea form-control summernote" placeholder="Place some text here"
                style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                name="text">{{ $item[0]->text }}</textarea>
            <span class="text-danger">{{ $errors->first('text') }}</span>
        </div>
        <div class="form-group">
            <label>Расм</label>
            <x-adminlte-input-file name="image" igroup-size="md" value="{{ $item[0]->image }}" type="file"
                placeholder="Расмни юклаш...">
                <x-slot name="prependSlot">
                    <div class="input-group-text bg-lightblue">
                        <i class="fas fa-upload"></i>
                    </div>
                </x-slot>
            </x-adminlte-input-file>
        </div>

        <button type="submit" class="btn btn-primary float-right"><i
                class="fa fa-check"></i>&nbsp;&nbsp;{{ __('Маълумотни сақлаш') }}</button>
    </form>
@stop
@section('js')
    <script src="{{ asset('/vendor/summernote/summernote-bs4.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.summernote').summernote({
                height: 300
            });
        });
    </script>
@stop
