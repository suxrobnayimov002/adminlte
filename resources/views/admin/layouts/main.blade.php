@extends('adminlte::page')

@section('title', 'Dashboard')
<link rel="shortcut icon"  href="{{ asset('img/android-chrome-192x192.png') }}" >

@section('content_header')
    <h2>Dashboard</h2>
@stop

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    @if(Session::has('message'))
                        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                    @endif
                </div>
                <div class="box-body">
                    @section('content')
                </div>
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" href="{{ asset('/css/adminlte.min.css') }}">
    @stop
    
    @section('js')
    <script src="{{ asset('js/bs-custom-file-input.min.js') }}"></script>
    <script> console.log('Hi!'); </script>
    <script>
        $(function () {
          bsCustomFileInput.init();
        });
        </script>
@stop
