@extends('admin.layouts.main')

@section('title', 'Профиль')

@section('content_header')
    <h4>Профиль</h4>
@stop

@section('content')
    <div class="wrapper">
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3">
                        <div class="card card-primary card-outline">
                            <div class="card-body box-profile">
                                <div class="text-center">
                                    <img class="profile-user-img img-fluid img-circle" src="/img/ortiq.jpg"
                                        alt="User profile picture">
                                </div>

                                <h3 class="profile-username text-center">Бакаев Ортик</h3>

                                <p class="text-muted text-center">Менеджер</p>

                                <ul class="list-group list-group-unbordered mb-3">
                                    <li class="list-group-item">
                                        <b>Келиб тушган аризалар</b> <a class="float-right">1,322</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Кўриб чиқилганлар</b> <a class="float-right">543</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Рад етилганлар</b> <a class="float-right">13,287</a>
                                    </li>
                                </ul>

                                <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>
                            </div>
                        </div>
                        
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Мен хақимда</h3>
                            </div>
                            <div class="card-body">
                                <strong><i class="fas fa-book mr-1"></i> Education</strong>

                                <p class="text-muted">
                                    B.S. in Computer Science from the University of Tennessee at Knoxville
                                </p>

                                <hr>

                                <strong><i class="fas fa-map-marker-alt mr-1"></i> Location</strong>

                                <p class="text-muted">Malibu, California</p>

                                <hr>

                                <strong><i class="fas fa-pencil-alt mr-1"></i> Skills</strong>

                                <p class="text-muted">
                                    <span class="tag tag-danger">UI Design</span>
                                    <span class="tag tag-success">Coding</span>
                                    <span class="tag tag-info">Javascript</span>
                                    <span class="tag tag-warning">PHP</span>
                                    <span class="tag tag-primary">Node.js</span>
                                </p>
                                <hr>
                                <strong><i class="far fa-file-alt mr-1"></i> Notes</strong>

                                <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
                                    fermentum enim neque.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="card">
                            <div class="card-header p-2">
                                <ul class="nav nav-pills">
                                    <li class="nav-item"><a class="nav-link active" href="#activity"
                                            data-toggle="tab">Activity</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#timeline"
                                            data-toggle="tab">Timeline</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#settings"
                                            data-toggle="tab">Settings</a></li>
                                </ul>
                            <div class="card-body">
                                <div class="tab-content">
                                    <div class="active tab-pane" id="activity">
                                        @foreach ($messages as $item)
                                            <div class="post">
                                                <div class="user-block">
                                                    <img class="img-circle img-bordered-sm"
                                                        src="https://picsum.photos/128/128?random" alt="user image">
                                                    <span class="username">
                                                        <a href="#">{{ $item->fullname }}</a>
                                                        <p>{{ $item->phone }}</p>
                                                        <form action="{{ route('profile.destroy', $item->id) }}"
                                                            method="post"
                                                            onsubmit="return confirm('Сиз ростдан ҳам ушбу маълумотни ўчиришни хохлайсизми ?')">
                                                            @csrf
                                                            @method('delete')
                                                            <span href="#" class="float-right btn-tool"
                                                                style="cursor: pointer" aria-label="Close"><i
                                                                    class="fas fa-times " ></i></span>
                                                        </form>
                                                    </span>
                                                    <span class="description">{{ $item->created_at }}</span>
                                                </div>
                                                <p>{{ $item->message }}</p>
                                                <p>
                                                    <a href="#" class="link-black text-sm mr-2"><i
                                                            class="fas fa-share mr-1"></i> Share</a>
                                                    <a href="#" class="link-black text-sm"><i
                                                            class="far fa-thumbs-up mr-1"></i> Like</a>
                                                    <span class="float-right">
                                                        <a href="#" class="link-black text-sm">
                                                            <i class="far fa-comments mr-1"></i> Comments (5)
                                                        </a>
                                                    </span>
                                                </p>

                                                <input class="form-control form-control-sm" type="text"
                                                    placeholder="Type a comment">
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="pagination mt-5 text-center w-100">
                                        {{ $messages->links() }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
    </div>
@endsection
