@extends('admin.layouts.main')

@section('title', __('Янги қўшилган маълумотни таҳрирлаш'))
<link rel="stylesheet" href="{{ asset('/administrator/summernote/summernote-bs4.css')  }}">

@section('content_header')
    <h4>{{ __('Янги қўшилган маълумотни таҳрирлаш') }}</h4>
@stop

@section('content')

    <form action="{{ route('projects.update', $item[0]->id) }}" method="POST" enctype="multipart/form-data">
        @csrf()
        @method('PUT')
        <div class="form-group">
            <label>Ф.И.О</label>
            <input class="form-control @error('pr_title') is-invalid @else is-valid @enderror" name="pr_title" value="{{ $item[0]->pr_title }}">
            <span class="text-danger">{{ $errors->first('pr_title') }}</span>
        </div>
        <div class="form-group">
            <label>Қисқача маълумот</label>
            <textarea class="textarea form-control summernote" placeholder="Place some text here"
                style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                name="pr_text">{{ $item[0]->pr_text }}</textarea>
            <span class="text-danger">{{ $errors->first('pr_text') }}</span>
        </div>
        <div class="form-group">
            <label>Расм</label>
            <input class="form-control @error('pr_image') is-invalid @else is-valid @enderror" type="file" name="pr_image" value="{{ $item[0]->pr_image }}" id="image">
            <span class="text-danger">{{ $errors->first('pr_image') }}</span> 
        </div>

        <button type="submit" class="btn btn-primary float-right"><i class="fa fa-check"></i>&nbsp;&nbsp;{{ __('Маълумотни сақлаш') }}</button>
    </form>
@stop
@section('js')
    <script src="{{ asset('/administrator/summernote/summernote-bs4.min.js')  }}"></script>
    <script>
        $(document).ready(function() {
            $('.summernote').summernote({
                height: 300,
            });
        });
    </script>
@stop
