@extends('admin.layouts.main')

@section('title', 'Проект қўшиш')
<link rel="stylesheet" href="{{ asset('/vendor/summernote/summernote-bs4.css') }}">

@section('content_header')
    <h4>Проект қўшиш</h4>
@stop

@section('content')

    <form action="{{ route('projects.store') }}" method="post" enctype="multipart/form-data">
        @csrf()
        <div class="form-group">
            <label>Сарлавҳа номи</label>
            <input class="form-control @error('pr_title') is-invalid @else is-valid @enderror" name="pr_title">
            <span class="text-danger">{{ $errors->first('pr_title') }}</span>
            @error('pr_title')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label>Қисқача маълумот</label>
            <textarea id="editor1" class="textarea form-control summernote" placeholder="Place some text here"
                style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                name="pr_text">
                </textarea>
            <span class="text-danger">{{ $errors->first('pr_text') }}</span>
        </div>
        <div class="form-group">
            <label>Расм</label>
            <input class="form-control @error('pr_image') is-invalid @else is-valid @enderror" type="file" name="pr_image" id="image">
            <span class="text-danger">{{ $errors->first('pr_image') }}</span> 
        </div>
        <button type="submit" class="btn btn-primary btn-md float-right"><i class="fa fa-check"></i>
            {{ __('Маълумотни сақлаш') }}</button>
    </form>
@stop
@section('js')
    <script src="{{ asset('/vendor/summernote/summernote-bs4.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.summernote').summernote({
                height: 300
            });
        });
    </script>
@stop
