@extends('admin.layouts.main')

@section('title', 'Сервис')

@section('content_header')
<h4>Сервис</h4>
@stop

@section('content')
<div class="card card-outline card-info">
    <div class="card-header">
        <h3 class="card-title">
            <a href="{{ route('service.create') }}" class="btn btn-primary btn-md float-right"><span class="fas fa-fw fa-plus"></span>Маълумот қўшиш
            </a>
        </h3>
    </div>
    <div class="card-body pad">
        <div class="mb-3">
            <table cellpadding="0" cellspacing="0" id="example" class="table table-bordered table-striped table-condensed">
                <thead>
                    <tr class="text-info">
                        <th>№</th>
                        <th>Сарлавҳа номи</th>
                        <th>Матн киритинг</th>
                        <th>Расм</th>
                        <th class="actions">Амаллар</th>
                    </tr>
                </thead>
                <tbody>

                    <?php $i = 1;?>
                    @foreach($services as $item)
                    {{-- <?php dd($item) ?> --}}
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $item->service_title }} </td>
                        <td>{{ $item->service_text }} </td>
                        <td style="width: 140px;"><img src="{{ $item->service_logo }}" alt="" width="100%" height="100"></td>
                        <td>
                            <a href="{{ route('portfolio.edit', $item->id) }}" class="btn btn-info btn-sm">
                                <span class="fas fa-fw fa-pencil-alt"></span>
                            </a>
                            <form action="{{ route('service.destroy',  $item->id) }}" method="post" onsubmit="return confirm('Сиз ростдан ҳам ушбу маълумотни ўчиришни хохлайсизми ?')">
                                @csrf
                                @method('delete')
                                <button type="submit" class="btn btn-sm btn-danger">
                                    <span class="fas fa-fw fa-trash-alt"></span>
                                </button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    {{ $services->links() }}
</div>
@stop
@section('js')
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });

</script>
@stop
