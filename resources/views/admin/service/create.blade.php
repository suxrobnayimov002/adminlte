@extends('admin.layouts.main')

@section('title', 'Сервис қўшиш')
<link rel="stylesheet" href="{{ asset('/vendor/summernote/summernote-bs4.css') }}">

@section('content_header')
    <h4>Сервис қўшиш</h4>
@stop

@section('content')

    <form action="{{ route('service.store') }}" method="post" enctype="multipart/form-data">
        @csrf()
        <div class="form-group">
            <label>Сарлавҳа номи</label>
            <input class="form-control @error('service_title') is-invalid @else is-valid @enderror" name="service_title">
            <span class="text-danger">{{ $errors->first('service_title') }}</span>

        </div>
        <div class="form-group">
            <label>Матн киритинг</label>
            <textarea id="editor1 @error('service_text') is-invalid @else is-valid @enderror"
                class="textarea form-control summernote" placeholder="Place some text here"
                style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                name="service_text">
                    </textarea>
            <span class="text-danger">{{ $errors->first('service_text') }}</span>
        </div>
        <div class="form-group">
            <label>Расм</label>
            <input class="form-control @error('service_logo') is-invalid @else is-valid @enderror" type="file"
                name="service_logo"">
            </div>
            <button type=" submit" class="btn btn-primary btn-md float-right"><i class="fa fa-check"></i>
            {{ __('Маълумотни сақлаш') }}</button>
    </form>
@stop
@section('js')
    <script src="{{ asset('/vendor/summernote/summernote-bs4.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.summernote').summernote({
                height: 300
            });
        });
    </script>
@stop
