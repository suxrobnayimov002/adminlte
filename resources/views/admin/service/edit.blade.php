@extends('admin.layouts.main')

@section('title', __('Янги қўшилган ма\ълумотни таҳрирлаш'))
<link rel="stylesheet" href="{{ asset('/vendor/summernote/summernote-bs4.css') }}">

@section('content_header')
    <h4>{{ __('Янги қўшилган ма\ълумотни таҳрирлаш') }}</h4>
@stop

@section('content')
    <form action="{{ route('service.update', $item[0]->id) }}" method="POST" enctype="multipart/form-data">
        {{-- <?php dd($item[0]->text); ?> --}}
        @csrf()
        @method('PUT')
        <div class="form-group">
            <label>Сарлавҳа номи</label>
            <input name="service_title" class="form-control @error('service_title') is-invalid @else is-valid @enderror"
                value="{{ $item[0]->service_title }}">
        </div>
        <div class="form-group">
            <label>Матн киритинг</label>
            <textarea class="textarea form-control summernote @error('service_text') is-invalid @else is-valid @enderror"
                placeholder="Place some text here"
                style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                name="service_text">{{ $item[0]->service_text }}</textarea>
            <span class="text-danger">{{ $errors->first('service_text') }}</span>
        </div>
        <div class="form-group">
            <label>Расм</label>
            <input type="file" name="service_logo"
                class="form-control
                @error('service_logo') is-invalid @else is-valid @enderror"
                value="{{ $item[0]->service_logo }}">
        </div>

        <button type="submit" class="btn btn-primary float-right"><i
                class="fa fa-check"></i>&nbsp;&nbsp;{{ __('Маълумотни сақлаш') }}</button>
    </form>
@stop
@section('js')
    <script src="{{ asset('/vendor/summernote/summernote-bs4.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.summernote').summernote({
                height: 300
            });
        });
    </script>
@stop
