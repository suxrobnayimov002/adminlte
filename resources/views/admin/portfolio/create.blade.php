@extends('admin.layouts.main')

@section('title', 'Портфолио қўшиш')
<link rel="stylesheet" href="{{ asset('/vendor/summernote/summernote-bs4.css') }}">

@section('content_header')
    <h4>Портфолио қўшиш</h4>
@stop

@section('content')

    <form action="{{ route('portfolio.store') }}" method="post" enctype="multipart/form-data">
        @csrf()
        <div class="form-group">
            <label>Сарлавҳа номи</label>
            <input class="form-control @error('title') is-invalid @else is-valid @enderror" name="title">
            <span class="text-danger">{{ $errors->first('title') }}</span>
            @error('title')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label>Матн киритинг</label>
            <textarea id="editor1" class="textarea form-control summernote" placeholder="Place some text here"
                style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                name="text">
                </textarea>
            <span class="text-danger">{{ $errors->first('text_all') }}</span>
        </div>
        <div class="form-group">
            <label>Расм</label>
            <x-adminlte-input-file name="image" igroup-size="md" id="image" type="file" placeholder="Расмни юклаш...">
                <x-slot name="prependSlot">
                    <div class="input-group-text bg-lightblue">
                        <i class="fas fa-upload"></i>
                    </div>
                </x-slot>
            </x-adminlte-input-file>
        </div>
        <button type="submit" class="btn btn-primary btn-md float-right"><i class="fa fa-check"></i>
            {{ __('Маълумотни сақлаш') }}</button>
    </form>
@stop
@section('js')
    <script src="{{ asset('/vendor/summernote/summernote-bs4.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.summernote').summernote({
                height: 300
            });
        });
    </script>
@stop
