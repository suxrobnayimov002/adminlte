@extends('admin.layouts.main')

@section('title', 'Бизнинг жамоа')

@section('content_header')
    <h4>Бизнинг жамоа</h4>
@stop

@section('content')
    <div class="card card-outline card-info">
        <div class="card-header">
            <h3 class="card-title">
                <a href="{{ route('teams.create') }}" class="btn btn-primary btn-md float-right"><span
                        class="fas fa-fw fa-plus"></span>Аъзо қўшиш
                </a>
            </h3>
        </div>
        <div class="card-body pad">
            <div class="mb-3">
                <table cellpadding="0" cellspacing="0" id="example" class="table table-bordered table-striped table-condensed">
                    <thead>
                    <tr class="text-info">
                        <th>№</th>
                        <th>Расм</th>
                        <th>Ф.И.О</th>
                        <th>Телефон рақам</th>
                        <th>Лавозим</th>

                        <th class="actions">Амаллар</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i = 1;?>
                    @foreach($teams as $item)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td style="width: 120px;"><img src="{{ $item->image }}" alt="" width="80%" height="100"></td>
                            <td>{{ $item->fullname }} </td>
                            <td>{{ $item->phone }} </td>
                            <td>{{ $item->position }} </td>
                            <td>
                                <a href="{{ route('teams.edit', $item->id) }}" class="btn btn-info btn-sm">
                                    <span class="fas fa-fw fa-pencil-alt"></span>
                                </a>
                                <form action="{{ route('teams.destroy',  $item->id) }}" method="post"
                                    onsubmit="return confirm('Сиз ростдан ҳам ушбу маълумотни ўчиришни хохлайсизми ?')">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="btn btn-sm btn-danger">
                                        <span class="fas fa-fw fa-trash-alt"></span>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    {{ $teams->links() }}
    </div>
@stop
@section('js')
    <script>
        $(document).ready(function () {
            $('#example').DataTable();
        });
    </script>
@stop
