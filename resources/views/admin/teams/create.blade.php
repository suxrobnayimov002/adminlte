@extends('admin.layouts.main')

@section('title', 'Маълумот қўшиш')
<link rel="stylesheet" href="{{ asset('/administrator/summernote/summernote-bs4.css') }}">

@section('content_header')
    <h4>Маълумот қўшиш</h4>
@stop

@section('content')

    @if (Session::has('success'))
        <div class="alert alert-success text-center">
            {{ Session::get('success') }}
        </div>
    @endif

    <form action="{{ route('teams.store') }}" method="post" enctype="multipart/form-data">
        @csrf()
        <div class="form-group">
            <label class="form-label">Ф.И.О</label>
            <input class="form-control @error('full_name') is-invalid @enderror" name="full_name">
            <span class="text-danger">{{ $errors->first('full_name') }}</span>
        </div>
        <div class="form-group">
            <label>Лавозим</label>
            <input class="form-control  @error('position') is-invalid @else is-valid @enderror" name="position">
            <span class="text-danger">{{ $errors->first('position') }}</span>
        </div>
        <div class="form-group">
            <label>Телефон рақам</label>

            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-phone"></i></span>
                </div>
                <input class="form-control  @error('phone') is-invalid @else is-valid @enderror" type="text" name="phone"
                    id="phone" data-inputmask='"mask": "(999) 99-999-99-99"' data-mask> <br>
            </div>
            <span class="text-danger">{{ $errors->first('phone') }}</span>
        </div>
        <div class="form-group">
            <label>Расм</label>
            <input class="form-control @error('image') is-invalid @else is-valid @enderror" type="file" name="image"">
                <span class=" text-danger">{{ $errors->first('image') }}</span>
        </div>
        <button type="submit" class="btn btn-primary btn-md float-right"><i class="fa fa-check"></i>
            {{ __('Маълумотни сақлаш') }}</button>
    </form>

@stop

@section('js')
    <script>
        $(document).ready(function() {

            $('.phone').inputmask('(999)-999-9999');

        });
    </script>
@stop
