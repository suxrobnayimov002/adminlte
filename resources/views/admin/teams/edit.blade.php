@extends('admin.layouts.main')

@section('title', __('Янги қўшилган маълумотни таҳрирлаш'))
<link rel="stylesheet" href="{{ asset('/administrator/summernote/summernote-bs4.css')  }}">

@section('content_header')
    <h4>{{ __('Янги қўшилган маълумотни таҳрирлаш') }}</h4>
@stop

@section('content')

    <form action="{{ route('teams.update', $team[0]->id) }}" method="POST" enctype="multipart/form-data">
        @csrf()
        @method('PUT')
        <div class="form-group">
            <label>Ф.И.О</label>
            <input class="form-control @error('full_name') is-invalid @else is-valid @enderror" name="full_name" value="{{ $team[0]->fullname }}">
            <span class="text-danger">{{ $errors->first('full_name') }}</span>
        </div>
        <div class="form-group">
            <label>Лавозим</label>
            <input class="form-control @error('position') is-invalid @else is-valid @enderror" name="position" value="{{ $team[0]->position }}">
            <span class="text-danger">{{ $errors->first('position') }}</span>
        </div>
        <div class="form-group">
            <label>Телефон рақам</label>
            <input class="form-control @error('phone') is-invalid @else is-valid @enderror" name="phone" value="{{ $team[0]->phone }}">
        </div>
        <div class="form-group">
            <label>Расм</label>
            <input class="form-control @error('image') is-invalid @else is-valid @enderror" type="file" name="image" value="{{ $team[0]->image }}" id="image">
            <span class="text-danger">{{ $errors->first('image') }}</span> 
        </div>

        <button type="submit" class="btn btn-primary float-right"><i class="fa fa-check"></i>&nbsp;&nbsp;{{ __('Маълумотни сақлаш') }}</button>
    </form>
@stop
@section('js')
    <script src="{{ asset('/administrator/summernote/summernote-bs4.min.js')  }}"></script>
    <script>
        $(document).ready(function() {
            $('.summernote').summernote({
                height: 300,
            });
        });
    </script>
@stop
