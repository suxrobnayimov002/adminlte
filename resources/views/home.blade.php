@extends('adminlte::page')

@section('title', 'Stroy')
<link rel="shortcut icon" href="{{ asset('img/logo.svg') }}">
@section('content_header')
    <h1 class="m-0 text-dark">Cтроительство тизимига хуш келибсиз!!!</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <p class="mb-0">Ушбу тизимга маълумотларингизни киритиб сиз ўз сайтингизни тузинг!</p>
                </div>
            </div>
        </div>
    </div>
@stop
