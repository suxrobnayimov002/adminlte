@extends('layouts.main')

@section('content')
<!-- Preloader -->
<div class="loader">
    <div class="loader-inner">
        <div class="k-line k-line11-1"></div>
        <div class="k-line k-line11-2"></div>
        <div class="k-line k-line11-3"></div>
        <div class="k-line k-line11-4"></div>
        <div class="k-line k-line11-5"></div>
    </div>
</div>
<!-- End Preloader -->

@include('layouts.includes.navbar')

@include('layouts.includes.about_us')

@include('layouts.includes.portfolio')



<!-- Start Count Down -->
<section id="countdown" class="section" data-stellar-background-ratio="0.3">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.4s">
                <!-- Single Count -->
                <div class="single-count">
                    <i class="fa fa-tasks"></i>
                    <h2><span class="count">50</span>K</h2>
                    <p>{{ __('menu.finished_project') }}</p>
                </div>
                <!--/ End Single Count --> 
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.6s">
                <!-- Single Count -->
                <div class="single-count">
                    <i class="fa fa-users"></i>
                    <h2><span class="count">650</span>K</h2>
                    <p>{{ __('menu.happy_clients') }}</p>
                </div>
                <!--/ End Single Count -->
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.8s">
                <!-- Single Count -->
                <div class="single-count active">
                    <i class="fa fa-clock-o"></i>
                    <h2><span class="count">500</span>K</h2>
                    <p>{{ __('menu.hours_worked') }}</p>
                </div>
                <!--/ End Single Count -->
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="1s">
                <!-- Single Count -->
                <div class="single-count">
                    <i class="fa fa-coffee"></i>
                    <h2><span class="count">100</span>K</h2>
                    <p>{{ __('menu.process') }}</p>
                </div>
                <!--/ End Single Count -->
            </div>
        </div>
    </div>
</section>
<!--/ End Count Down -->

<!-- Start Timeline-->
<section id="my-timeline" class="section clearfix">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="section-title">
                    <h1><span>{{ __('menu.projects') }}</span><i class="fa fa-history"></i></h1>
                    <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old<p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="timeline">
                    <div class="timeline-inner">
                        <!-- Single Timeline -->
                        <div class="single-main wow fadeInLeft" data-wow-delay="0.4s">
                            <div class="single-timeline">
                                <div class="single-content">
                                    <div class="date">
                                        <p>Jan<span>10</span></p>
                                    </div>
                                    <h2>High School Degree</h2>
                                    <p>Interdum et malesuada fames ac ante ipsum primis in faucibus. Etiam efficitur ultricies elit, eget ullamcorper enim scelerisque sit amet. Pellentesque blandit venenatis rhoncus.<p>
                                </div>
                            </div>
                        </div>
                        <!--/ End Single Timeline -->
                        <!-- Single Timeline -->
                        <div class="single-main wow fadeInRight" data-wow-delay="0.6s">
                            <div class="single-timeline">
                                <div class="single-content">
                                    <div class="date">
                                        <p>Dec<span>12</span></p>
                                    </div>
                                    <h2>Start Web Design</h2>
                                    <p>Interdum et malesuada fames ac ante ipsum primis in faucibus. Etiam efficitur ultricies elit, eget ullamcorper enim scelerisque sit amet. Pellentesque blandit venenatis rhoncus.<p>
                                </div>
                            </div>
                        </div>
                        <!--/ End Single Timeline -->
                        <!-- Single Timeline -->
                        <div class="single-main wow fadeInLeft" data-wow-delay="0.4s">
                            <div class="single-timeline">
                                <div class="single-content">
                                    <div class="date">
                                        <p>Jan<span>13</span></p>
                                    </div>
                                    <h2>Join University</h2>
                                    <p>Starting College DayInterdum et malesuada fames ac ante ipsum primis in faucibus. Etiam efficitur ultricies elit, eget ullamcorper enim scelerisque sit amet. Pellentesque blandit venenatis rhoncus.</p>
                                </div>
                            </div>
                        </div>
                        <!--/ End Single Timeline -->
                        <!-- Single Timeline -->
                        <div class="single-main wow fadeInRight" data-wow-delay="0.6s">
                            <div class="single-timeline">
                                <div class="single-content">
                                    <div class="date">
                                        <p>Feb<span>16</span></p>
                                    </div>
                                    <h2>Start Web Developments</h2>
                                    <p>Learn Web Developments Interdum et malesuada fames ac ante ipsum primis in faucibus. Etiam efficitur ultricies elit, eget ullamcorper enim scelerisque sit amet. Pellentesque blandit venenatis rhoncus.</p>
                                </div>
                            </div>
                        </div>
                        <!--/ End Single Timeline -->
                        <!-- Single Timeline -->
                        <div class="single-main wow fadeInLeft" data-wow-delay="0.4s">
                            <div class="single-timeline">
                                <div class="single-content">
                                    <div class="date">
                                        <p>Jan<span>17</span></p>
                                    </div>
                                    <h2>Complete Graduations</h2>
                                    <p>Just Receive my Interdum et malesuada fames ac ante ipsum primis in faucibus. Etiam efficitur ultricies elit, eget ullamcorper enim scelerisque sit amet. Pellentesque blandit venenatis rhoncus.</p>
                                </div>
                            </div>
                        </div>
                        <!--/ End Single Timeline -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/ End Timeline -->

<!-- Start Pricing Table -->
<section id="pricing" class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="section-title">
                    <h1><span>{{ __('menu.my-service') }}</span><i class="fa fa-history"></i></h1>
                    <p>ontrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old<p>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- Single Table -->
            <div class="col-md-4 col-sm-12 col-xs-12 wow fadeIn" data-wow-delay="0.4s">
                <div class="single-table">
                    <!-- Table Head -->
                    <div class="table-head">
                        <h2 class="title">Basic</h2>
                        <div class="price">
                            <p class="amount">$<span>50</span>/Year</p>
                        </div>
                        <i class="fa fa-gift"></i>
                    </div>
                    <!-- Table List -->
                    <ul class="table-list">
                        <li>1 Business website</li>
                        <li>24/7 Technic Support</li>
                        <li>User Live</li>
                        <li>Unlimited Email Account</li>
                        <li class="cross">Disk Space</li>
                    </ul>
                    <!-- Table Bottom -->
                    <div class="table-bottom">
                        <a class="btn shine" href="#">Buy Now<i class="fa fa-angle-right"></i></a>
                    </div>
                </div>
            </div>
            <!-- End Single Table-->
            <!-- Single Table -->
            <div class="col-md-4 col-sm-12 col-xs-12 wow fadeIn" data-wow-delay="0.6s">
                <div class="single-table active">
                    <!-- Table Head -->
                    <div class="table-head">
                        <h2 class="title">Standard</h2>
                        <div class="price">
                            <p class="amount">$<span>80</span>/Year</p>
                        </div>
                        <i class="fa fa-gift"></i>
                    </div>
                    <!-- Table List -->
                    <ul class="table-list">
                        <li>2 Business website</li>
                        <li>24/7 Technic Support</li>
                        <li>User Live</li>
                        <li>Unlimited Email Account</li>
                        <li class="cross">Disk Space</li>
                    </ul>
                    <!-- Table Bottom -->
                    <div class="table-bottom">
                        <a class="btn shine" href="#">Buy Now<i class="fa fa-angle-right"></i></a>
                    </div>
                </div>
            </div>
            <!-- End Single Table-->
            <!-- Single Table -->
            <div class="col-md-4 col-sm-12 col-xs-12 wow fadeIn" data-wow-delay="0.8s">
                <div class="single-table">
                    <!-- Table Head -->
                    <div class="table-head">
                        <h2 class="title">Premium</h2>
                        <div class="price">
                            <p class="amount">$<span>99</span>/Year</p>
                        </div>
                        <i class="fa fa-gift"></i>
                    </div>
                    <!-- Table List -->
                    <ul class="table-list">
                        <li>5 Business website</li>
                        <li>24/7 Technic Support</li>
                        <li>User Live</li>
                        <li>Unlimited Email Account</li>
                        <li class="cross">Disk Space</li>
                    </ul>
                    <!-- Table Bottom -->
                    <div class="table-bottom">
                        <a class="btn shine" href="#">Buy Now<i class="fa fa-angle-right"></i></a>
                    </div>
                </div>
            </div>
            <!-- End Single Table-->
        </div>
    </div>
</section>
<!--/ End Pricing Table -->

<!-- Start Testimonials -->
<section id="testimonials" class="section" data-stellar-background-ratio="0.3">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="section-title">
                    <h1><span>Clients</span> Testimonials<i class="fa fa-star"></i></h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="testimonial-carousel">
                    <!-- Single Testimonial -->
                    <div class="single-testimonial">
                        <div class="testimonial-content">
                            <i class="fa fa-quote-left"></i>
                            <p>Vestibulum scelerisque, turpis a imperdiet consectetur, est turpis sollicitudin lorem, ac scelerisque libero velit nec odio. Aliquam nunc eros, ultricies non consequat at, porttitor at ante. Etiam lorem erat, vulputate nec pellentesque ut, tempor vel nisi. Suspendisse porta sem vel mauris semper consequat. Proin id aliquet felis. Aliquam justo augue, aliquet id vulputate ut, ultricies non orci.</p>
                        </div>
                        <div class="testimonial-info">
                            <img src="https://picsum.photos/270/300?random=13" class="img-circle" alt="">
                            <ul class="rating">
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                            </ul>
                            <h6>Shakil Hossain<span>Web Developer</span></h6>
                        </div>
                    </div>
                    <!--/ End Single Testimonial -->
                    <!-- Single Testimonial -->
                    <div class="single-testimonial">
                        <div class="testimonial-content">
                            <i class="fa fa-quote-left"></i>
                            <p>Vestibulum scelerisque, turpis a imperdiet consectetur, est turpis sollicitudin lorem, ac scelerisque libero velit nec odio. Aliquam nunc eros, ultricies non consequat at, porttitor at ante. Etiam lorem erat, vulputate nec pellentesque ut, tempor vel nisi. Suspendisse porta sem vel mauris semper consequat. Proin id aliquet felis. Aliquam justo augue, aliquet id vulputate ut, ultricies non orci.</p>
                        </div>
                        <div class="testimonial-info">
                            <img src="https://picsum.photos/270/300?random=14" class="img-circle" alt="">
                            <ul class="rating">
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                            </ul>
                            <h6>Rimon Hossain<span>Web Designer</span></h6>
                        </div>
                    </div>
                    <!--/ End Single Testimonial -->
                    <!-- Single Testimonial -->
                    <div class="single-testimonial">
                        <div class="testimonial-content">
                            <i class="fa fa-quote-left"></i>
                            <p>Vestibulum scelerisque, turpis a imperdiet consectetur, est turpis sollicitudin lorem, ac scelerisque libero velit nec odio. Aliquam nunc eros, ultricies non consequat at, porttitor at ante. Etiam lorem erat, vulputate nec pellentesque ut, tempor vel nisi. Suspendisse porta sem vel mauris semper consequat. Proin id aliquet felis. Aliquam justo augue, aliquet id vulputate ut, ultricies non orci.</p>
                        </div>
                        <div class="testimonial-info">
                            <img src="https://picsum.photos/270/300?random=18" class="img-circle" alt="">
                            <ul class="rating">
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                            </ul>
                            <h6>Climen Langle<span>Mobile Devoloper</span></h6>
                        </div>
                    </div>
                    <!--/ End Single Testimonial -->
                </div>
            </div>
        </div>
    </div>
</section>
<!--/ End Testimonials -->

<!-- Start Blog -->
{{-- <section id="blog" class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="section-title">
                    <h1><span>Latest</span> Blog<i class="fa fa-pencil"></i></h1>
                    <p>ontrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old<p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="blog-carousel">
                    <div class="single-blog">
                        <div class="blog-head">
                            <img src="https://picsum.photos/1200/800?random=15" alt="" class="img-responsive">
                            <div class="blog-link">
                                <a href="blog-single.html"><i class="fa fa-link"></i></a>
                            </div>
                        </div>
                        <div class="blog-info">
                            <div class="date">
                                <div class="day"><span>20</span>Jan</div>
                                <div class="year">2021</div>
                            </div>
                            <h2><a href="blog-single.html">Creative Portfolio template</a></h2>
                            <div class="meta">
                                <span><i class="fa fa-user"></i>By admin</span>
                                <span><i class="fa fa-comments"></i>45 comments</span>
                                <span><i class="fa fa-eye"></i>5k views</span>
                            </div>
                            <p>There are many variations of passages of Lorem Ipsum available. Donec libero ante, tempus vitae lacus a, sollicitudin ultrices augue. Sed bibendum ligula lectus, eget malesuada mauris. </p>
                            <a href="blog-single.html" class="btn">Read more<i class="fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
                    <div class="single-blog">
                        <div class="blog-head">
                            <img src="https://picsum.photos/1200/800?random=16" alt="" class="img-responsive">
                            <div class="blog-link">
                                <a href="blog-single.html"><i class="fa fa-link"></i></a>
                            </div>
                        </div>
                        <div class="blog-info">
                            <div class="date">
                                <div class="day"><span>28</span> Jun</div>
                                <div class="year">2021</div>
                            </div>
                            <h2><a href="blog-single.html">Creative Onepage template</a></h2>
                            <div class="meta">
                                <span><i class="fa fa-user"></i>By admin</span>
                                <span><i class="fa fa-comments"></i>15 comments</span>
                                <span><i class="fa fa-eye"></i>1k views</span>
                            </div>
                            <p>There are many variations of passages of Lorem Ipsum available. Donec libero ante, tempus vitae lacus a, sollicitudin ultrices augue. Sed bibendum ligula lectus, eget malesuada mauris. </p>
                            <a href="blog-single.html" class="btn">Read more<i class="fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
                    <div class="single-blog">
                        <div class="blog-head">
                            <img src="https://picsum.photos/1200/800?random=17" alt="" class="img-responsive">
                            <div class="blog-link">
                                <a href="blog-single.html"><i class="fa fa-link"></i></a>
                            </div>
                        </div>
                        <div class="blog-info">
                            <div class="date">
                                <div class="day"><span>1</span> July</div>
                                <div class="year">2021</div>
                            </div>
                            <h2><a href="blog-single.html">Onepage Portfolio Website</a></h2>
                            <div class="meta">
                                <span><i class="fa fa-user"></i>By admin</span>
                                <span><i class="fa fa-comments"></i>450 comments</span>
                                <span><i class="fa fa-eye"></i>33k views</span>
                            </div>
                            <p>There are many variations of passages of Lorem Ipsum available. Donec libero ante, tempus vitae lacus a, sollicitudin ultrices augue. Sed bibendum ligula lectus, eget malesuada mauris. </p>
                            <a href="blog-single.html" class="btn">Read more<i class="fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
                    <div class="single-blog">
                        <div class="blog-head">
                            <img src="https://picsum.photos/1200/800?random=18" alt="" class="img-responsive">
                            <div class="blog-link">
                                <a href="blog-single.html"><i class="fa fa-link"></i></a>
                            </div>
                        </div>
                        <div class="blog-info">
                            <div class="date">
                                <div class="day"><span>20</span>Jan</div>
                                <div class="year">2021</div>
                            </div>
                            <h2><a href="blog-single.html">Creative Portfolio template</a></h2>
                            <div class="meta">
                                <span><i class="fa fa-user"></i>By admin</span>
                                <span><i class="fa fa-comments"></i>45 comments</span>
                                <span><i class="fa fa-eye"></i>5k views</span>
                            </div>
                            <p>There are many variations of passages of Lorem Ipsum available. Donec libero ante, tempus vitae lacus a, sollicitudin ultrices augue. Sed bibendum ligula lectus, eget malesuada mauris. </p>
                            <a href="blog-single.html" class="btn">Read more<i class="fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
                    <div class="single-blog">
                        <div class="blog-head">
                            <img src="https://picsum.photos/1200/800?random=19" alt="" class="img-responsive">
                            <div class="blog-link">
                                <a href="blog-single.html"><i class="fa fa-link"></i></a>
                            </div>
                        </div>
                        <div class="blog-info">
                            <div class="date">
                                <div class="day"><span>28</span> Jun</div>
                                <div class="year">2017</div>
                            </div>
                            <h2><a href="blog-single.html">Creative Onepage template</a></h2>
                            <div class="meta">
                                <span><i class="fa fa-user"></i>By admin</span>
                                <span><i class="fa fa-comments"></i>15 comments</span>
                                <span><i class="fa fa-eye"></i>1k views</span>
                            </div>
                            <p>There are many variations of passages of Lorem Ipsum available. Donec libero ante, tempus vitae lacus a, sollicitudin ultrices augue. Sed bibendum ligula lectus, eget malesuada mauris. </p>
                            <a href="blog-single.html" class="btn">Read more<i class="fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
                    <div class="single-blog">
                        <div class="blog-head">
                            <img src="https://picsum.photos/1200/800?random=20" alt="" class="img-responsive">
                            <div class="blog-link">
                                <a href="blog-single.html"><i class="fa fa-link"></i></a>
                            </div>
                        </div>
                        <div class="blog-info">
                            <div class="date">
                                <div class="day"><span>1</span> July</div>
                                <div class="year">2017</div>
                            </div>
                            <h2><a href="blog-single.html">Onepage Portfolio Website</a></h2>
                            <div class="meta">
                                <span><i class="fa fa-user"></i>By admin</span>
                                <span><i class="fa fa-comments"></i>450 comments</span>
                                <span><i class="fa fa-eye"></i>33k views</span>
                            </div>
                            <p>There are many variations of passages of Lorem Ipsum available. Donec libero ante, tempus vitae lacus a, sollicitudin ultrices augue. Sed bibendum ligula lectus, eget malesuada mauris. </p>
                            <a href="blog-single.html" class="btn">Read more<i class="fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> --}}
<!--/ End Blog -->

<!-- Start Call To Action -->
<section id="call-action" class="section wow fadeInUp">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="call-action-main">
                    <h2>I'm Ready for your next project</h2>
                    <p>Nam eleifend, turpis ac laoreet tincidunt, arcu purus ultricies nisl, quis molestie nibh lacus et dui. Nunc efficitur turpis lorem, bibendum dictum libero ornare a. Proin quis metus massa. Aliquam erat volutpat. Nullam eu convallis arcu, sit amet pulvinar odio. Nulla facilisi. Nullam ac nibh ac lectus aliquet aliquam. Interdum et malesuada fames ac ante ipsum primis in faucibus. Etiam sed ipsum non tortor posuere rutrum. </p>
                    <div class="button">
                        <a href="#" class="btn"><i class="fa fa-address-book"></i>Hire ME Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/ End Call To Action -->

@include('layouts.includes.contact')

<!-- Start Clients -->
<div id="clients" class="section" data-stellar-background-ratio="0.3">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="clients-slider">
                    <!-- Single Clients -->
                    <div class="single-clients">
                        <a href="#" target="_blank"><img src="/img/clients-1.png" alt="#"></a>
                    </div>
                    <!--/ End Single Clients -->
                    <!-- Single Clients -->
                    <div class="single-clients active">
                        <a href="#" target="_blank"><img src="/img/clients-2.png" alt="#"></a>
                    </div>
                    <!--/ End Single Clients -->
                    <!-- Single Clients -->
                    <div class="single-clients">
                        <a href="#" target="_blank"><img src="/img/clients-3.png" alt="#"></a>
                    </div>
                    <!--/ End Single Clients -->
                    <!-- Single Clients -->
                    <div class="single-clients">
                        <a href="#" target="_blank"><img src="/img/clients-4.png" alt="#"></a>
                    </div>
                    <!--/ End Single Clients -->
                    <!-- Single Clients -->
                    <div class="single-clients">
                        <a href="#" target="_blank"><img src="/img/clients-5.png" alt="#"></a>
                    </div>
                    <!--/ End Single Clients -->
                    <!-- Single Clients -->
                    <div class="single-clients">
                        <a href="#" target="_blank"><img src="/img/clients-6.png" alt="#"></a>
                    </div>
                    <!--/ End Single Clients -->
                </div>
            </div>
        </div>
    </div>
</div>
<!--/ End Clients -->


@endsection
