@extends('layouts.main')

@section('additionalCss')

@endsection

@section('content')
<div class="management">
    <div class="management-information">
        <img src="/img/bg6.jpg" alt="">
        <div class="container">
            <h1 class="teamTitle text-center">{{ __('menu.our-team') }}</span></i></h1>
            <nav class="teambreadcrumb__list" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">{{ __('menu.home') }}</a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{ __('menu.our-team') }}</li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="container">
        <div class="management-image">
            <div class="managemet-items">
                @foreach($teams as $item)
                    <div class="management-list">
                        <img src="{{ $item->image }}">
                        <h4>{{ $item->fullname }}</h4>
                        <h5>{{ $item->position }}</h5>
                        <div class="management-info">
                            <ul class="management-info-items mt-3 ">
                            </ul>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
