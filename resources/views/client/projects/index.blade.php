@extends('layouts.main')

@section('additionalCss')

@endsection

@section('content')

<!-- Start Timeline-->
<section id="my-timeline" class="section clearfix">
<div class="service-backgrond">
        <img class="picsum-img" src="/img/bg6.jpg">
        <div class="container">
            <h1 class="projectTitle">{{ __('menu.projects') }}</span></i></h1>
            <nav class="breadcrumb__list" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">{{ __('menu.home') }}</a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{ __('menu.projects') }}</li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="section-title mt-5">
                    <h1><span>{{ __('menu.projects') }}</span><i class="fa fa-history"></i></h1>
                    <p>ontrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old<p>
                </div>
            </div>
            <img src="/img/team-img.jpeg" style="margin-bottom: 30px;" alt="">
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="timeline">
                    <div class="timeline-inner">
                        <!-- Single Timeline -->
                        <div class="single-main wow fadeInLeft" data-wow-delay="0.4s">
                            <div class="single-timeline">
                                <div class="single-content">
                                    <div class="date">
                                        <p>Jan<span>10</span></p>
                                    </div>
                                    <h2>High School Degree</h2>
                                    <p>Interdum et malesuada fames ac ante ipsum primis in faucibus. Etiam efficitur ultricies elit, eget ullamcorper enim scelerisque sit amet. Pellentesque blandit venenatis rhoncus.<p>
                                </div>
                            </div>
                        </div>
                        <!--/ End Single Timeline -->
                        <!-- Single Timeline -->
                        <div class="single-main wow fadeInRight" data-wow-delay="0.6s">
                            <div class="single-timeline">
                                <div class="single-content">
                                    <div class="date">
                                        <p>Dec<span>12</span></p>
                                    </div>
                                    <h2>Start Web Design</h2>
                                    <p>Interdum et malesuada fames ac ante ipsum primis in faucibus. Etiam efficitur ultricies elit, eget ullamcorper enim scelerisque sit amet. Pellentesque blandit venenatis rhoncus.<p>
                                </div>
                            </div>
                        </div>
                        <!--/ End Single Timeline -->
                        <!-- Single Timeline -->
                        <div class="single-main wow fadeInLeft" data-wow-delay="0.4s">
                            <div class="single-timeline">
                                <div class="single-content">
                                    <div class="date">
                                        <p>Jan<span>13</span></p>
                                    </div>
                                    <h2>Join University</h2>
                                    <p>Starting College DayInterdum et malesuada fames ac ante ipsum primis in faucibus. Etiam efficitur ultricies elit, eget ullamcorper enim scelerisque sit amet. Pellentesque blandit venenatis rhoncus.</p>
                                </div>
                            </div>
                        </div>
                        <!--/ End Single Timeline -->
                        <!-- Single Timeline -->
                        <div class="single-main wow fadeInRight" data-wow-delay="0.6s">
                            <div class="single-timeline">
                                <div class="single-content">
                                    <div class="date">
                                        <p>Feb<span>16</span></p>
                                    </div>
                                    <h2>Start Web Developments</h2>
                                    <p>Learn Web Developments Interdum et malesuada fames ac ante ipsum primis in faucibus. Etiam efficitur ultricies elit, eget ullamcorper enim scelerisque sit amet. Pellentesque blandit venenatis rhoncus.</p>
                                </div>
                            </div>
                        </div>
                        <!--/ End Single Timeline -->
                        <!-- Single Timeline -->
                        <div class="single-main wow fadeInLeft" data-wow-delay="0.4s">
                            <div class="single-timeline">
                                <div class="single-content">
                                    <div class="date">
                                        <p>Jan<span>17</span></p>
                                    </div>
                                    <h2>Complete Graduations</h2>
                                    <p>Just Receive my Interdum et malesuada fames ac ante ipsum primis in faucibus. Etiam efficitur ultricies elit, eget ullamcorper enim scelerisque sit amet. Pellentesque blandit venenatis rhoncus.</p>
                                </div>
                            </div>
                        </div>
                        <!--/ End Single Timeline -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<style>
    .section {
        padding: 0;
    }
    .section-title {
        padding: 60px 0;
    }
</style>
@endsection
