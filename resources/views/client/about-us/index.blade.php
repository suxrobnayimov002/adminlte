@extends('layouts.main')

@section('additionalCss')
@endsection

@section('content')
    <!-- Start About Me -->
    <section id="about-me" class="section allAbout">
        <div>
            <img class="picsum-img" src="/img/bg1.jpg" height="350px">
            <div class=" container">
                <h1 class="aboutTitle">All information {{ __('menu.about_us') }}</i></h1>
                  <nav aria-label="breadcrumb" class="breadcrumb_list">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="/">{{ __('menu.home') }}</a></li>
                      <li class="breadcrumb-item active" aria-current="page">{{ __('menu.about_us') }}</li>
                    </ol>
                  </nav>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 fix mb-5">
                    <ul class="about_me">
                        <li class="nav-item" style="width: 50%">
                            <h4 class="pb-3"><span class="font-weight-500">We understand the importance of</span> innovation and
                                professionalism</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta repellat veritatis inventore
                                iste beatae porro doloremque autem mollitia natus, hic voluptatibus neque molestiae
                                perferendis eum aliquam, sit in nulla blanditiis? <br> Assumenda laborum rerum consequatur
                                dolor sequi necessitatibus nulla qui quam sint nobis inventore, non obcaecati ea asperiores
                                laudantium soluta voluptatem vel officia fuga ad quas voluptas nihil! Reprehenderit, sunt
                                aliquam velit obcaecati sit cumque eaque tempore facere maiores iste sequi blanditiis
                                reiciendis molestias nam iure deserunt. Quia totam, est vel voluptatibus expedita et magni
                                eligendi doloribus consectetur? Nisi ratione magnam accusantium error illo consectetur.
                                Explicabo fugiat iusto excepturi at eveniet!</p>
                        </li>
                        <li class="nav-item">
                            <img class="about-img" src="/img/about-main.png" width="550px" height="660px">

                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    </style>
    <!--/ End About Me -->
    <style>
        .section {
            padding: 0px !important;
        }

    </style>
@endsection
