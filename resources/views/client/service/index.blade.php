@extends('layouts.main')

@section('additionalCss')

@endsection

@section('content')
<!-- Start Service -->
<section id="my-service" class="section">
    <div class="service-backgrond">
        <img class="picsum-img" src="/img/bg4.jpg">
        {{-- <h2 > </h2> --}}
        <div class="container">
            <h1 class="serviceTitle text-center">{{ __('menu.my-service') }}</span></i></h1>
            <nav class="servicebreadcrumb_list" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">{{ __('menu.home') }}</a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{ __('menu.my-service') }}</li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="section-title" style="margin-top: 50px;">
                    <h1><span>{{ __('menu.my-service') }}</span> <i class="fa fa-rocket"></i></h1>
                    <p>ontrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old<p>
                </div>
            </div>
        </div>
        <div class="row" style="padding-bottom: 30px">
            <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-delay="0.6s">
                <!-- Single Service -->
                <div class="hover"></div>
                <div class="single-service">
                    <i class="fa fa-home"></i>
                    <h2>Web Development</h2>
                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some</p>
                </div>
                <!-- End Single Service -->
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-delay="0.6s">
                <!-- Single Service -->
                <div class="hover"></div>
                <div class="single-service">
                    <i class="fa fa-code"></i>
                    <h2>Web Development</h2>
                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some</p>
                </div>
                <!-- End Single Service -->
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-delay="0.8s">
                <!-- Single Service -->
                <div class="hover"></div>
                <div class="single-service">
                    <i class="fa fa-mobile"></i>
                    <h2>Responsive Design</h2>
                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some</p>
                </div>
                <!-- End Single Service -->
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-delay="1s">
                <!-- Single Service -->
                <div class="hover"></div>
                <div class="single-service">
                    <i class="fa fa-pencil-square-o"></i>
                    <h2>Customize Website</h2>
                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some</p>
                </div>
                <!-- End Single Service -->
            </div>
        </div>
    </div>
</section>
<section id="countdown" class="section" data-stellar-background-ratio="0.3">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.4s">
                <!-- Single Count -->
                <div class="single-count">
                    <i class="fa fa-tasks"></i>
                    <h2><span class="count">50</span>K</h2>
                    <p>Projects Finished</p>
                </div>
                <!--/ End Single Count -->
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.6s">
                <!-- Single Count -->
                <div class="single-count">
                    <i class="fa fa-users"></i>
                    <h2><span class="count">650</span>K</h2>
                    <p>Happy Clients</p>
                </div>
                <!--/ End Single Count -->
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.8s">
                <!-- Single Count -->
                <div class="single-count active">
                    <i class="fa fa-clock-o"></i>
                    <h2><span class="count">500</span>K</h2>
                    <p>Hours Worked</p>
                </div>
                <!--/ End Single Count -->
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="1s">
                <!-- Single Count -->
                <div class="single-count">
                    <i class="fa fa-coffee"></i>
                    <h2><span class="count">100</span>K</h2>
                    <p>Cups of Coffee</p>
                </div>
                <!--/ End Single Count -->
            </div>
        </div>
    </div>
</section>
<style>
    .section {
        padding: 0px !important;
    }

</style>
@endsection
