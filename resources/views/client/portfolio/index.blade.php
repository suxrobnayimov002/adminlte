@extends('layouts.main')

@section('additionalCss')

@endsection

@section('content')


<!-- Start portfolio -->
<section id="portfolio" class="section">
    <div class="service-backgrond">
        <img class="picsum-img" src="/img/section-bg6-1.jpg">
        <div class="container">
            <h1 id="team" class="management-title mt-4 mb-4">{{ __('menu.portfolio') }}</h1>
            <nav class="portfoliobreadcrumb__list" aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="/">{{ __('menu.home') }}</a></li>
                  <li class="breadcrumb-item active" aria-current="page">{{ __('menu.portfolio') }}</li>
                </ol>
              </nav>
        </div>
    </div>
    <div class="container-fluid container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="section-title">
                    <h1><span>{{ __('menu.portfolio') }}</span><i class="fa fa-briefcase"></i></h1>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <!-- portfolio Nav -->
                <div class="portfolio-nav">
                    <ul>
                        <li class="active" data-filter="*"><span>06</span><i class="fa fa-tasks"></i>All Works</li>
                        <li data-filter="2"><span>03</span><i class="fa fa-pencil"></i>Web Development</li>
                        <li data-filter="3"><span>02</span><i class="fa fa-paint-brush"></i>Web Design</li>
                        <li data-filter=".html5"><span>03</span><i class="fa fa-html5"></i>HTML5</li>
                        <li data-filter=".wordpress"><span>02</span><i class="fa fa-wordpress"></i>Wordpress</li>
                    </ul>
                </div>
                <!--/ End portfolio Nav -->
            </div>
        </div>

        <div class="portfolio-inner">
            <div class="row stylex container">
                <div class="isotop-active">
                    @foreach ($portfolios as $item)
                    <div class="mix design wordpress development col-md-4 col-sm-6 col-xs-12 col-fix">
                        <div class="portfolio-single">
                            <div class="portfolio-head">
                                <img src="{{ $item->image }}" height="300px" alt="" />
                            </div>
                            <div class="portfolio-hover">
                                <h4>{{ $item->title }}</h4>
                                <p>{{ $item->text }}</p>
                                <div class="button">
                                    <a data-fancybox="gallery" href="{{ $item->image }}"><i class="fa fa-search"></i></a>
                                    <a href="#" class="primary"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
<style>
    #portfolio .container-fluid {
        padding: 70px 0 !important;
    }

    .section {
        padding: 0px !important;
    }
    .stylex, .portfolio-inner {
        margin: 0 auto;
    }
    .isotop-active {
        justify-content: space-between;
    }

</style>
<!--/ End portfolio -->
@endsection
