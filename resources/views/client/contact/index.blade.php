@extends('layouts.main')

@section('additionalCss')

@endsection

@section('content')
<!-- Start Contact -->
<section id="contact" class="section">
    <div class="contact-backgrond">
        <img class="picsum_img" src="/img/section-bg5.jpg">
        <div class="contactList container">
            <h1 class="contactTitle">{{ __('menu.contact') }}</span></i></h1>
              <nav class="contactbreadcrumb__list" aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="/">{{ __('menu.home') }}</a></li>
                  <li class="breadcrumb-item active" aria-current="page">{{ __('menu.contact') }}</li>
                </ol>
              </nav>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 ">
                <div class="section-title mt-5">
                    <h1><span>{{ __('menu.contact') }}</span><i class="fa fa-star"></i></h1>
                    <p>Montrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece <br> of classical Latin literature from 45 BC, making it over 2000 years old<p>
                </div>
            </div>
        </div>
        @if (session('success'))
            <div class="col-sm-8">
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>Успех!</strong> Ваш запрос успешно выполнен
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
            </div>
        @endif

        @if (session('error'))
            <div class="col-sm-8">
                <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                    {{ session('error') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                </div>
            </div>
        @endif
        <div class="row">
            <!-- Contact Form -->
            <div class="col-md-6 col-sm-6 col-xs-12 wow fadeInLeft" data-wow-delay="0.4s">
                <form class="form" method="post" action="{{ route('profile.index') }}" enctype="multipart/form-data">
                    @csrf()
                    <div class="row " style="padding: 20px">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" name="fullname" placeholder="Ф.И.О" required="required">
                                @foreach ($errors->all() as $error)
                                 <span><p> {{ $error }}</p></span>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="email" name="email" placeholder="Email" required="required">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="phone" name="phone" placeholder="Телефон" required="required">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <textarea name="message" rows="5" placeholder="Сообщение"></textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group button">
                                <button type="submit" class="button primary toastrDefaultSuccess"><i class="fa fa-send"></i>{{ __('menu.send') }}</button>
                            </div>
                        </div>
                    </div>
                </form>
                {{-- @if ($errors->any())
                    <div class="col-sm-12">
                        <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                            @foreach ($errors->all() as $error)
                                <span><p>{{ $error }}</p></span>
                            @endforeach
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
                    </div>
                @endif --}}

                
            </div>
            
            <!--/ End Contact Form -->
            <!-- Contact Address -->
            <div class="col-md-6 col-sm-6 col-xs-12 wow fadeInRight" data-wow-delay="0.8s">
                <div class="contact">
                    <!-- Single Address -->
                    <div class="single-address">
                        <i class="fa fa-phone"></i>
                        <div class="title">
                            <h4>{{ __('menu.phone') }}</h4>
                            <p>+998 (91) 649-03-79,<br>+998 (88) 860-82-87</p>
                        </div>
                    </div>
                    <!--/ End Single Address -->
                    <!-- Single Address -->
                    <div class="single-address">
                        <i class="fa fa-envelope"></i>
                        <div class="title">
                            <h4>{{ __('menu.email_address') }}</h4>
                            <p>myproject@gmail.com</p>
                        </div>
                    </div>
                    <!--/ End Single Address -->
                    <!-- Single Address -->
                    <div class="single-address">
                        <i class="fa fa-map"></i>
                        <div class="title">
                            <h4>{{ __('menu.location') }}</h4>
                            <p>12/27 Yunusobod, <br>Uzbekistan, Toshkent</p>
                        </div>
                    </div>
                    <!--/ End Single Address -->
                </div>
            </div>
            <!--/ End Contact Address -->
           

        </div>
    </div>
</section>
<!--/ End Contact -->
<style>
    .section {
        padding: 50px 0 !important;
    }
    #footer-top {
        margin-top: 220px
    }
    .section-title {
        padding: 30px 0;
    }
    .picsum_img, .contact-backgrond {
        height: 330px;
        width: 100%;
        object-fit: cover
    }
    .alert-success {
        position: absolute;
        right: 0;
        top: 95px;
    }
</style>

@endsection
