<!-- Footer Top -->
<section id="footer-top" class="section footer-newsletter">
    <div class="container">
        <div class="row" style="padding: 40px 0">
            <!-- Single Widget -->
            <div class="col-md-12 col-sm-12 col-xs-12 ">
                <div class="newslatter">
                    <h2>{{ __('menu.newslatter_text') }}</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio tenetur, dolores aperiam, quasi perferendis nemo mollitia <br> labore molestias modi consequatur expedita ad voluptates dolor ex. Impedit quo temporibus, molestiae fugit?</p>
                </div>
                <form class="news-form">
                    <input type="email" placeholder="type your email">
                    <button type="submit" class="button primary"><i class="fa fa-paper-plane"></i></button>
                </form>
            </div>
            <!--/ End Single Widget -->
        </div>
    </div>
</section>
<!--/ End footer Top -->

<!-- Start Footer -->
<footer id="footer">
    <div class="footer-items">
        <div class="container">
            <div class="footer_logo">
                <div class="footer-list">
                    <a href="" class="logo_site">
                        <img  src="/img/logo.svg" alt="">
                        <p style="padding-left: 10px; color: white; font-size: 23px;">
                            {{ __('menu.builder') }}
                        </p>
                    </a>
                    <p class="footer_list_text">With over 10 years of experience in construction, we partner with owners and design professionals to build high-quality projects.</p>
                        <!-- Social -->
                        <ul class="social">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li class="active"><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        </ul>
                        <!--/ End Social -->
                </div>
                <div class="footer-list" style="padding: 0px 0px 0 50px">
                    <h4 style="color: white;">{{ __('menu.useful_links') }}</h4>
                    <ul class="useful_links-item">
                        <ul class="useful_link">
                            <li style="padding: 10px 0">
                                <a href="">{{ __('menu.about_us') }}</a><br>
                            </li>
                            <li style="padding: 10px 0">
                                <a href="">{{ __('menu.my-service') }}</a><br>
                            </li>
                            <li style="padding: 10px 0">
                                <a href="">{{ __('menu.portfolio') }}</a>
                            </li>
                        </ul>
                        <ul class="useful_link" style="padding: 0px 20px 0 50px">
                            <li style="padding: 10px 0">
                                <a href="">{{ __('menu.teams') }}</a><br>
                            </li>
                            <li style="padding: 10px 0">
                                <a href="">{{ __('menu.projects') }}</a><br>
                            </li>
                            <li style="padding: 10px 0">
                                <a href="">{{ __('menu.contact') }}</a>
                            </li>
                        </ul>
                    </ul>
                </div>
                <div class="footer-list">
                    <h4 style="color: white; padding: 20px 20px 20px 50px">{{ __('menu.contact') }}</h4>
                    <ul class="useful_links-item">
                        <ul class="useful_link" style="padding: 0px 20px 0 50px">
                            <li class="contact-text" style="padding: 10px 0">
                                <i class="fa fa-clock-o"></i>
                                <p class="contact_list">Mon - Fri: 8.00am 6.00pm</p>
                            </li>
                            <li class="contact-text" style="padding: 10px 0">
                                <i class="fa fa-map-marker"></i>
                                <p class="contact_list">112 W 34th St, New York</p>
                            </li>
                            <li class="contact-text" style="padding: 10px 0">
                                <i class="fa fa-phone"></i>
                                <p class="contact_list">(+1) 212-946-2707</p>
                            </li>
                            <li class="contact-text" style="padding: 10px 0">
                                <i class="fa fa-envelope"></i>
                                <a href="" style="padding: 0px 0px 0 5px">info@Text.com</a>
                            </li>
                        </ul>
                    </ul>
                </div>
                <div class="footer-list">
                    <h4 style="color: white; padding: 20px 20px 20px 50px">{{ __('menu.newslatter_text') }}</h4>
                    <form class="news-form">
                        <input type="email" placeholder="Type your email">
                        <button type="submit" class="button subscribe-btn">Subscribe!</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Arrow -->
    <div id="button" class="arro scrollToTopBtn">
        <a href=""><i class="fa fa-angle-up angle__up"></i></a>
    </div>
    <!--/ End Arrow -->
    <div class="container ">
        <div class="row" style="padding: 20px 0 5px">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <!-- Copyright -->
                <div class="copyright">
                    <p>&copy; Construct 2021 Suxrob. Barcha huquqlar himoyalangan <a href="">construct.uz </a></p>
                </div>
                <!--/ End Copyright -->
            </div>

        </div>
    </div>
</footer>
