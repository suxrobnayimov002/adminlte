<!-- Start Personal Area -->
<section id="personal-area">
    <div class="personal-main">
        <div class="personal-single">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <!-- Personal Text -->
                        <div class="personal-text">
                            <div class="my-info">
                                <h1>My Name Is Suxrob</h1>
                                <h2 class="cd-headline clip is-full-width">
                                    МОЕ МЕСТО РАБОТЫ
                                    <span class="cd-words-wrapper">
                                        <b class="is-visibl">MEHNAT.UZ</b>
                                    </span>
                                </h2>
                                <div class="button">
                                    <a href="{{ route('client.contact.index') }}" class="btn primary shine"><i class="fa fa-rocket"></i>{{ __('menu.contact') }}</a>
                                    <a href="{{ route('client.projects.index') }}" class="btn shine"><i class="fa fa-briefcase"></i>{{ __('menu.projects') }}</a>
                                </div>
                            </div>
                        </div>
                        <!--/ End Personal Text -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/ End Personal Area -->
