<!-- Start portfolio -->
<section id="portfolio" class="section">
    <div class="container-fluid container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="section-title">
                    <h1><span>{{ __('menu.portfolio') }}</span><i class="fa fa-briefcase"></i></h1>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <!-- portfolio Nav -->
                <div class="portfolio-nav">
                    <ul>
                        <li class="active" data-filter="*"><span>06</span><i class="fa fa-tasks"></i>All Works</li>
                        <li data-filter="2"><span>03</span><i class="fa fa-pencil"></i>Web Development</li>
                        <li data-filter="3"><span>02</span><i class="fa fa-paint-brush"></i>Web Design</li>
                        <li data-filter=".html5"><span>03</span><i class="fa fa-html5"></i>HTML5</li>
                        <li data-filter=".wordpress"><span>02</span><i class="fa fa-wordpress"></i>Wordpress</li>
                    </ul>
                </div>
                <!--/ End portfolio Nav -->
            </div>
        </div>

        <div class="portfolio-inner">
            <div class="row stylex container">
                <div class="isotop-active">
                </div>
            </div>
        </div>
        <div class="row">
            <!-- Button -->
            <div class="button">
                <a href="{{ route('client.portfolio.index') }}" class="btn">{{ __('menu.portfolio_all') }}<i class="fa fa-angle-double-right"></i></a>
            </div>
            <!-- End Button -->
        </div>
    </div>
</section>
<!--/ End portfolio -->
