<!-- Start Header -->
<header id="header">
    <div class="container">

        <div class="row header_list">
            <div class="col-md-2 col-sm-12 col-xs-12">
                <!-- Logo -->
                <div class="logo">
                    <a href="/" class="logo_site">
                        {{-- <img src="/img/logo-white.png" alt=""> --}}
                        <img  src="/img/logo.svg" alt="">
                        <p style="padding-left: 10px;">
                            {{ __('menu.builder') }}
                        </p>
                    </a>
                </div>
                <!--/ End Logo -->
                <div class="mobile-nav"></div>
            </div>
            <div class="col-md-10 col-sm-12 col-xs-12">
                <div class="nav-area">
                    <!-- Main Menu -->
                    <nav class="mainmenu">
                        <div class="navbar">
                            <ul class="nav navbar-na menu">
                                <li >
                                    <a href="{{ route('client.about-us.index') }}" class="active">
                                        <i class="fa fa-home"></i>
                                        {{ __('menu.about_us') }}
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('client.service.index') }}">
                                        <i class="fa fa-rocket"></i>
                                        {{ __('menu.my-service') }}
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('client.portfolio.index') }}">
                                        <i class="fa fa-briefcase"></i>
                                        {{ __('menu.portfolio') }}
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('client.team.index') }}">
                                        <i class="fa fa-star"></i>
                                        {{ __('menu.teams') }}
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('client.projects.index') }}">
                                        <i class="fa fa-pencil"></i>
                                        {{ __('menu.projects') }}
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('client.contact.index') }}">
                                        <i class="fa fa-address-book"></i>
                                        {{ __('menu.contact') }}
                                    </a>
                                </li>
                            </ul>
                            <ul class="social-icon">
                                <li>
                                    <a href="#">
                                        <i class="bi bi-globe" style="font-size: 21px" title="Язык"></i>
                                    </a>
                                </li>
                            </ul>
                            <ul class="social">
                                @foreach (LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                    <li class="nav-item pad-l">
                                        <a class="nav-link " rel="alternate" hreflang="{{ $localeCode }}"
                                            href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                            {{ $properties['native'] }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </nav>
                    <!--/ End Main Menu -->
                </div>
            </div>
        </div>
    </div>
</header>
<!--/ End Header -->
