<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name='copyright' content='codeglim'>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @yield('title')
    <title>Строитель</title>
    @yield('additionalCss')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">

    <link rel="shortcut icon"  href="{{ asset('img/android-chrome-192x192.png') }}" >

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">

    <!-- Animate CSS -->
    <link rel="stylesheet" href="{{ asset('/css/animate.min.css') }}">

    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="{{ asset('/css/font-awesome.min.css') }}">

    <!-- Fancy Box CSS -->
    <link rel="stylesheet" href="{{ asset('/css/jquery.fancybox.min.css') }}">

    <!-- Slick Nav CSS -->
    <link rel="stylesheet" href="{{ asset('/css/slicknav.min.css') }}">

    <!-- Animate Text -->
    <link rel="stylesheet" href="{{ asset('/css/animate.text.css') }}">

    <!-- Owl Carousel CSS -->
    <link rel="stylesheet" href="{{ asset('/css/owl.theme.default.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/owl.carousel.min.css') }}">

    <!-- Bootstrap Css -->
    <link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}">

    <!-- Sufia StyleShet -->
    <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/reset.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/toastr.min.css') }}">

    <link rel="stylesheet" href="{{ asset('/css/responsive.css') }}">

    <!-- Maheraj Template Color -->
    <link rel="stylesheet" href="{{ asset('/css/color/color1.css') }}">

    {{-- <link rel="stylesheet" href="{{ asset('/css/adminlte.min.css') }}"> --}}

    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

</head>
<body>

    @include('layouts.includes.header')
    @yield('content')
    @include('layouts.includes.footer')


    <!-- Jquery -->
    <script src="{{ asset('js/jquery.min.js')  }}"></script>
    <script src="{{ asset('js/jquery.inputmask.min.js') }}"></script>

    <!-- Modernizr JS -->
    <script src="{{ asset('js/modernizr.min.js')  }}"></script>

    <!-- WOW JS -->
    <script src="{{ asset('js/wow.min.js')  }}"></script>

    <!-- Fancybox js -->
    <script src="{{ asset('js/jquery.fancybox.min.js')  }}"></script>

    <!-- Animate Text JS -->
    <script src="{{ asset('js/select2.full.min.js')  }}"></script>

    <!-- Mobile Menu JS -->
    <script src="{{ asset('js/jquery.slicknav.min.js')  }}"></script>

    <!-- Jquery Parallax -->
    <script src="{{ asset('js/jquery.stellar.min.js')  }}"></script>

    <!-- Jquery Easing -->
    <script src="{{ asset('js/easing.js')  }}"></script>

    <!-- One Page Nav JS -->
    <script src="{{ asset('js/jquery.nav.js')  }}"></script>

    <!-- Slider Carousel JS -->
    <script src="{{ asset('js/owl.carousel.min.js')  }}"></script>

    <!-- Youtube Player JS -->
    <script src="{{ asset('js/ytplayer.min.js')  }}"></script>

    <!-- Particle JS -->
    <script src="{{ asset('js/particles.min.js')  }}"></script>

    <!-- Counter JS -->
    <script src="{{ asset('js/waypoints.min.js')  }}"></script>
    <script src="{{ asset('js/jquery.counterup.min.js')  }}"></script>

    <!-- Mixitup JS -->
    <script src="{{ asset('js/isotope.pkgd.min.js')  }}"></script>

    <!-- Bootstrap JS -->
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js')}}"></script>

    <!-- Main JS -->
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="{{ asset('js/adminlte.min.js') }}"></script>
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <!-- </script> -->
    <script src="{{ asset('js/bs-custom-file-input.min.js') }}"></script>

    @yield('additionalJS')
</body>
</html>
