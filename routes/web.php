<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AboutController;
use App\Http\Controllers\Admin\PortfolioController;
use App\Http\Controllers\Admin\ServicesController;
use App\Http\Controllers\Admin\TeamController;
use App\Http\Controllers\Admin\ProjectsController;
use App\Http\Controllers\Admin\ProfileController;
use App\Http\Controllers\Admin\EditProfileController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
    ], function(){

        Route::get('/', [\App\Http\Controllers\IndexController::class, 'index'])->name('index');
        Route::get('/teams', [\App\Http\Controllers\TeamController::class, 'team'])->name('client.team.index');
        Route::get('/abouts', [\App\Http\Controllers\AboutUsController::class, 'about'])->name('client.about-us.index');
        Route::get('/portfolio', [\App\Http\Controllers\PortfoliosController::class, 'portfolio'])->name('client.portfolio.index');
        Route::get('/service', [\App\Http\Controllers\ServiceController::class, 'service'])->name('client.service.index');
        Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home')->where('name', '[A-Za-z]+');
        Route::get('/project', [App\Http\Controllers\ProjectController::class, 'project'])->name('client.projects.index');
        Route::get('/contact', [App\Http\Controllers\ContactController::class, 'contact'])->name('client.contact.index')->where('name', '[A-Za-z]+');
        Route::get('/mail', [App\Http\Controllers\MailController::class, 'sendEmail'])->name('emails.mail');

});

Route::middleware(['auth:web'])->group(function () {

    Route::group(['prefix' => 'admin'], function () {

        Route::resources([
            'teams'      => TeamController::class,
            'abouts'     => AboutController::class,
            'portfolio'  => PortfolioController::class,
            'service'    => ServicesController::class,
            'projects'   => ProjectsController::class,
            'profile'    => ProfileController::class,
            // 'edit-profile'    => EditProfileController::class,
        ]);

    });

});
